import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { HttpClient, HttpHeaders } from '@angular/common/http';


import 'rxjs/add/operator/map';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  private messageSource = new BehaviorSubject(0);
 //public url='http://localhost:3000';
  public domain ='http://api.checkyourpropertyvalue.com';


  currentMessage = this.messageSource.asObservable();



  changeMessage(provider){
   // console.log(provider);
    this.messageSource.next(provider);
  }


  Sendemail(body) {
    return this.http.post(this.domain + '/authentication/register', body);
  }


}
