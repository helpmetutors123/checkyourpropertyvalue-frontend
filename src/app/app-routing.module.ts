import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PolicyComponent } from './policy/policy.component';
import { Submit1Component } from './submit1/submit1.component';
import { Submit2Component } from './submit2/submit2.component';
import { Submit3Component } from './submit3/submit3.component';
import { ThankyouComponent } from './thankyou/thankyou.component';



const routes: Routes = [
  { path: '', component: HomeComponent},
  {path:'submit1', component:Submit1Component},
  {path:'submit2', component:Submit2Component},
  {path:'submit3', component:Submit3Component},
  {path:'thankyou', component:ThankyouComponent},{path:'policy', component:PolicyComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
