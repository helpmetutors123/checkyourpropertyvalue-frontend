import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import { RecaptchaFormsModule } from 'ng-recaptcha';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { AgmCoreModule } from '@agm/core';
import { LayoutModule } from '@angular/cdk/layout';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSelectModule} from '@angular/material/select';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FlexLayoutModule } from '@angular/flex-layout';

import { Submit2Component } from './submit2/submit2.component';
import { Submit3Component } from './submit3/submit3.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { Submit1Component } from './submit1/submit1.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { AuthService } from './auth.service';
import { PolicyComponent } from './policy/policy.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    Submit2Component,
    Submit3Component,
    ThankyouComponent,
    Submit1Component,
    PolicyComponent,

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GooglePlaceModule,
    BrowserAnimationsModule,
      MatCardModule, 
      LayoutModule,
      MatButtonModule,
      FormsModule,
      ReactiveFormsModule,
      MatInputModule,
      MatTableModule,
      MatGridListModule,
      MatSelectModule,
      MatCheckboxModule,
      HttpClientModule,
      FlexLayoutModule,
      RecaptchaModule,
      RecaptchaFormsModule,
      NgxIntlTelInputModule,
      AgmCoreModule.forRoot({
        apiKey: 'AIzaSyBPsEPDo-FyhRwqY9v2sF7pI-I7k_Ju1U4'})
    
  ],
  providers: [AuthService,
    {provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: '6LeEcNUZAAAAADieAw-akSax0pmwlMD7yejVmYDU',
    } as RecaptchaSettings}],
  bootstrap: [AppComponent]
})
export class AppModule { }
