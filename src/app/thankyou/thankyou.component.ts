import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {
  formattedAddress ="";
  constructor(
    private authService:AuthService
  ) { }

  options ={
    componentRestrictions: {
      country : ['IND', 'CA','USA']
    }
  }
  public handleAddressChange(address: any) {
  this.formattedAddress = address.formatted_address;
  console.log(this.formattedAddress);
}
  
  ngOnInit(): void {
    let data= {
      "email":JSON.parse(localStorage.getItem('data2')).email,
      "name":JSON.parse(localStorage.getItem('data2')).name,
      "location":JSON.parse(localStorage.getItem('useraddress')),
      "mobile":JSON.parse(localStorage.getItem('data2')).phone,
      "bedrooms":JSON.parse(localStorage.getItem('data1')).bedrooms,
      "selltime":JSON.parse(localStorage.getItem('data1')).sell,
      "sqfeet":JSON.parse(localStorage.getItem('data1')).sq,
      "bath":JSON.parse(localStorage.getItem('data1')).bath,
      "unit":JSON.parse(localStorage.getItem('unitdata')).unit
    }
    console.log(data);
    this.authService.Sendemail(data).subscribe(data=>{
      window.alert('Your details are on the way to our team');
    })
  }



}
