import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-submit3',
  templateUrl: './submit3.component.html',
  styleUrls: ['./submit3.component.scss']
})
export class Submit3Component implements OnInit {
  

  form: FormGroup;
 
  
   email = new FormControl('', [Validators.required, Validators.email]);
   //tel = new FormControl (null, [Validators.required, Validators.pattern("[0-9 ]{12}")]);
   phone= new FormControl('', [Validators.required]);
   name = new FormControl('',Validators.required);
   recaptchaReactive= new FormControl('', Validators.required)


  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }




  constructor(
    private router:Router,
    private formBuilder: FormBuilder

  ) { }

  ngOnInit(): void {


    this.form = this.formBuilder.group({
      name: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      phone: [null,[Validators.required]],
     // phone: new FormControl('', [Validators.required]),
      recaptchaReactive: [null, Validators.required],
    });
  }


onSecondSubmit(){
  console.log(this.email.value, this.phone.value, this.name.value);
  console.log(this.form.value)
  const data2={
    "email":this.form.value.email,
    "phone":this.form.value.phone,
    "name":this.form.value.name
  } 
  console.log(data2);
  localStorage.setItem('data2',JSON.stringify(data2));
  this.router.navigate(['/thankyou']);
}
}
