import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators ,FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService} from  '../auth.service';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  formattedAddress ="";
  unit = new FormControl('', [Validators.required]);

  cvcForm: FormGroup;
  latitude;
  longitude
  constructor(
    private router:Router,
    private authService:AuthService,
    private metaTagService: Meta

  ) { }

  
  options ={
    componentRestrictions: {
      country : ['IND', 'CA','USA']
    }
  }
  public handleAddressChange(address: any) {
  this.formattedAddress = address.formatted_address;
 
  }


onButtonClick(){
  if(this.formattedAddress==''){
    }else{



var geocoder = new google.maps.Geocoder();
var address = "new york";

geocoder.geocode( { 'address':this.formattedAddress }, ((results, status)=> {

  if (status == google.maps.GeocoderStatus.OK) {
     this.latitude = results[0].geometry.location.lat();
   this.longitude = results[0].geometry.location.lng();

   const data ={
    "lat":this.latitude,
    "long":this.longitude,
  }

    this.authService.changeMessage(data)

} 

 

})); 

localStorage.setItem('useraddress', JSON.stringify(this.formattedAddress));

const data={
  "unit":this.unit.value
}
localStorage.setItem('unitdata', JSON.stringify(data));
this.router.navigate(['/submit1']);



}

}





  ngOnInit(): void {
    this.metaTagService.addTags([
      { name: 'keywords', content: 'Free Evaluation, GTA Condos Online, Condo Value Calculator, Toronto Condo Prices, How Much My Condo is Worth ' },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'Surinder singh' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'date', content: '2020-09-27', scheme: 'YYYY-MM-DD' },
      { charset: 'UTF-8' }
    ]);
  }

  

}