import { Component, OnInit } from '@angular/core';
import {AuthService} from  '../auth.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-submit1',
  templateUrl: './submit1.component.html',
  styleUrls: ['./submit1.component.scss']
})
export class Submit1Component implements OnInit {
  title = 'Condo Value Check Submit 1';

  //title = 'My first AGM project';
  lat ;
  lng;
  zoom: number = 20;
  sat = 'hybrid';
  unit;
  
  //mapType = 'satellite';
  constructor(
    private authService:AuthService,
    private titleService: Title,
    private metaTagService: Meta

  ) {

   }

  ngOnInit(){

      this.authService.currentMessage.subscribe(data=>{
        this.lat =data['lat'];
        this.lng = data['long'];    
    })

    this.titleService.setTitle(this.title);
    this.metaTagService.updateTag(
      { name: 'description', content: 'Add song template' }
    );

   
  
  }

}
