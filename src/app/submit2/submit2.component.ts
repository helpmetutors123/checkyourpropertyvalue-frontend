import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Router } from '@angular/router';




@Component({
  selector: 'app-submit2',
  templateUrl: './submit2.component.html',
  styleUrls: ['./submit2.component.scss']
})
export class Submit2Component implements OnInit {
  form: FormGroup;

  bedroomsControl = new FormControl('', [Validators.required]);

  bedroomss = [
    {name: '1'},
    {name: '2'},
    {name: '3'},
    {name: '4'},
    {name: '5'},
    {name:'more than 5'}
  ];


  bathroomsControl = new FormControl('', [Validators.required]);

  bathroomss = [
    {name: '1'},
    {name: '2'},
    {name: '3'},
    {name: '4'},
    {name: '5'},
    {name:'more than 5'}
  ];

  squarefeetControl = new FormControl('', [Validators.required]);
  
  squarefeets = [
    {name: '0-200'},
    {name: '200-400'},
    {name: '400-600'},
    {name: '600-800'},
    {name: '800-1000'},
    {name: '1000-1200'},
    {name: '1200-1400'},
    {name: '1400-1600'},
    {name: '1600-1800'},
    {name: '1800-2000'},
    {name: '2000-2200'},
    {name: '2200-2400'},
    {name:'more than 2400'}
  ];

  
  sellinginControl = new FormControl('', [Validators.required]);
 sellingins = [
    {name: '1-3 months'},
    {name: '3-6 months'},
    {name: '6-12 months'},
    {name: 'just curious'},
    {name: 'refinancing'},
  ];



  constructor(
    private router:Router,
    private formBuilder: FormBuilder
  ) { }





  ngOnInit(): void {
    this.form = this.formBuilder.group({
      bedroomsControl: [null, Validators.required],
      bathroomsControl: [null, Validators.required],
      squarefeetControl: [null, Validators.required],
      sellinginControl: [null, Validators.required],
    });
   
  }

  onFirstSubmit(){
   //console.log(this.form.value)
  const data1={
    "bedrooms":this.form.value.bedroomsControl,
    "sell":this.form.value.sellinginControl,
    "bath":this.form.value.bathroomsControl,
    "sq":this.form.value.squarefeetControl
  }
  //console.log(data1);
  localStorage.setItem('data1', JSON.stringify(data1));
  this.router.navigate(['/submit3']);

}
 
}

