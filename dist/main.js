(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _policy_policy_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./policy/policy.component */ "./src/app/policy/policy.component.ts");
/* harmony import */ var _submit1_submit1_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./submit1/submit1.component */ "./src/app/submit1/submit1.component.ts");
/* harmony import */ var _submit2_submit2_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./submit2/submit2.component */ "./src/app/submit2/submit2.component.ts");
/* harmony import */ var _submit3_submit3_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./submit3/submit3.component */ "./src/app/submit3/submit3.component.ts");
/* harmony import */ var _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./thankyou/thankyou.component */ "./src/app/thankyou/thankyou.component.ts");










const routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] },
    { path: 'submit1', component: _submit1_submit1_component__WEBPACK_IMPORTED_MODULE_4__["Submit1Component"] },
    { path: 'submit2', component: _submit2_submit2_component__WEBPACK_IMPORTED_MODULE_5__["Submit2Component"] },
    { path: 'submit3', component: _submit3_submit3_component__WEBPACK_IMPORTED_MODULE_6__["Submit3Component"] },
    { path: 'thankyou', component: _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_7__["ThankyouComponent"] }, { path: 'policy', component: _policy_policy_component__WEBPACK_IMPORTED_MODULE_3__["PolicyComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




class AppComponent {
    constructor(metaTagService) {
        this.metaTagService = metaTagService;
        this.title = 'Condovaluecheck';
    }
    ngOnInit() {
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Meta"] }]; }, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng-recaptcha */ "./node_modules/ng-recaptcha/__ivy_ngcc__/fesm2015/ng-recaptcha.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/__ivy_ngcc__/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/__ivy_ngcc__/fesm2015/agm-core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/layout.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/grid-list.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/__ivy_ngcc__/esm2015/flex-layout.js");
/* harmony import */ var _submit2_submit2_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./submit2/submit2.component */ "./src/app/submit2/submit2.component.ts");
/* harmony import */ var _submit3_submit3_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./submit3/submit3.component */ "./src/app/submit3/submit3.component.ts");
/* harmony import */ var _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./thankyou/thankyou.component */ "./src/app/thankyou/thankyou.component.ts");
/* harmony import */ var _submit1_submit1_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./submit1/submit1.component */ "./src/app/submit1/submit1.component.ts");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ngx-intl-tel-input */ "./node_modules/ngx-intl-tel-input/__ivy_ngcc__/fesm2015/ngx-intl-tel-input.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _policy_policy_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./policy/policy.component */ "./src/app/policy/policy.component.ts");






























class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [_auth_service__WEBPACK_IMPORTED_MODULE_25__["AuthService"], { provide: ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RECAPTCHA_SETTINGS"], useValue: {
                siteKey: '6LeEcNUZAAAAADieAw-akSax0pmwlMD7yejVmYDU',
            } }], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_7__["GooglePlaceModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__["BrowserAnimationsModule"],
            _angular_material_card__WEBPACK_IMPORTED_MODULE_15__["MatCardModule"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__["LayoutModule"],
            _angular_material_button__WEBPACK_IMPORTED_MODULE_16__["MatButtonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"],
            _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
            _angular_material_table__WEBPACK_IMPORTED_MODULE_11__["MatTableModule"],
            _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_12__["MatGridListModule"],
            _angular_material_select__WEBPACK_IMPORTED_MODULE_13__["MatSelectModule"],
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_19__["FlexLayoutModule"],
            ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RecaptchaModule"],
            ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RecaptchaFormsModule"],
            ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_24__["NgxIntlTelInputModule"],
            _agm_core__WEBPACK_IMPORTED_MODULE_8__["AgmCoreModule"].forRoot({
                apiKey: 'AIzaSyBPsEPDo-FyhRwqY9v2sF7pI-I7k_Ju1U4'
            })
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
        _submit2_submit2_component__WEBPACK_IMPORTED_MODULE_20__["Submit2Component"],
        _submit3_submit3_component__WEBPACK_IMPORTED_MODULE_21__["Submit3Component"],
        _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_22__["ThankyouComponent"],
        _submit1_submit1_component__WEBPACK_IMPORTED_MODULE_23__["Submit1Component"],
        _policy_policy_component__WEBPACK_IMPORTED_MODULE_26__["PolicyComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
        ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_7__["GooglePlaceModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__["BrowserAnimationsModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_15__["MatCardModule"],
        _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__["LayoutModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_16__["MatButtonModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_11__["MatTableModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_12__["MatGridListModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_13__["MatSelectModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
        _angular_flex_layout__WEBPACK_IMPORTED_MODULE_19__["FlexLayoutModule"],
        ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RecaptchaModule"],
        ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RecaptchaFormsModule"],
        ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_24__["NgxIntlTelInputModule"], _agm_core__WEBPACK_IMPORTED_MODULE_8__["AgmCoreModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                    _submit2_submit2_component__WEBPACK_IMPORTED_MODULE_20__["Submit2Component"],
                    _submit3_submit3_component__WEBPACK_IMPORTED_MODULE_21__["Submit3Component"],
                    _thankyou_thankyou_component__WEBPACK_IMPORTED_MODULE_22__["ThankyouComponent"],
                    _submit1_submit1_component__WEBPACK_IMPORTED_MODULE_23__["Submit1Component"],
                    _policy_policy_component__WEBPACK_IMPORTED_MODULE_26__["PolicyComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                    ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_7__["GooglePlaceModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__["BrowserAnimationsModule"],
                    _angular_material_card__WEBPACK_IMPORTED_MODULE_15__["MatCardModule"],
                    _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_9__["LayoutModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_16__["MatButtonModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_17__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_17__["ReactiveFormsModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
                    _angular_material_table__WEBPACK_IMPORTED_MODULE_11__["MatTableModule"],
                    _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_12__["MatGridListModule"],
                    _angular_material_select__WEBPACK_IMPORTED_MODULE_13__["MatSelectModule"],
                    _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_18__["MatCheckboxModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                    _angular_flex_layout__WEBPACK_IMPORTED_MODULE_19__["FlexLayoutModule"],
                    ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RecaptchaModule"],
                    ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RecaptchaFormsModule"],
                    ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_24__["NgxIntlTelInputModule"],
                    _agm_core__WEBPACK_IMPORTED_MODULE_8__["AgmCoreModule"].forRoot({
                        apiKey: 'AIzaSyBPsEPDo-FyhRwqY9v2sF7pI-I7k_Ju1U4'
                    })
                ],
                providers: [_auth_service__WEBPACK_IMPORTED_MODULE_25__["AuthService"], { provide: ng_recaptcha__WEBPACK_IMPORTED_MODULE_3__["RECAPTCHA_SETTINGS"], useValue: {
                            siteKey: '6LeEcNUZAAAAADieAw-akSax0pmwlMD7yejVmYDU',
                        } }],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs/BehaviorSubject.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");





class AuthService {
    constructor(http) {
        this.http = http;
        this.messageSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](0);
        //public url='http://localhost:3000';
        this.domain = 'http://api.condovaluecheck.com';
        this.currentMessage = this.messageSource.asObservable();
    }
    changeMessage(provider) {
        // console.log(provider);
        this.messageSource.next(provider);
    }
    Sendemail(body) {
        return this.http.post(this.domain + '/authentication/register', body);
    }
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"])); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-google-places-autocomplete */ "./node_modules/ngx-google-places-autocomplete/__ivy_ngcc__/bundles/ngx-google-places-autocomplete.umd.js");
/* harmony import */ var ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-intl-tel-input */ "./node_modules/ngx-intl-tel-input/__ivy_ngcc__/fesm2015/ngx-intl-tel-input.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");











class HomeComponent {
    constructor(router, authService, metaTagService) {
        this.router = router;
        this.authService = authService;
        this.metaTagService = metaTagService;
        this.formattedAddress = "";
        this.unit = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.options = {
            componentRestrictions: {
                country: ['IND', 'CA', 'USA']
            }
        };
    }
    handleAddressChange(address) {
        this.formattedAddress = address.formatted_address;
    }
    onButtonClick() {
        if (this.formattedAddress == '') {
        }
        else {
            var geocoder = new google.maps.Geocoder();
            var address = "new york";
            geocoder.geocode({ 'address': this.formattedAddress }, ((results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    this.latitude = results[0].geometry.location.lat();
                    this.longitude = results[0].geometry.location.lng();
                    const data = {
                        "lat": this.latitude,
                        "long": this.longitude,
                    };
                    this.authService.changeMessage(data);
                }
            }));
            localStorage.setItem('useraddress', JSON.stringify(this.formattedAddress));
            const data = {
                "unit": this.unit.value
            };
            localStorage.setItem('unitdata', JSON.stringify(data));
            this.router.navigate(['/submit1']);
        }
    }
    ngOnInit() {
        this.metaTagService.addTags([
            { name: 'keywords', content: 'Free Evaluation, GTA Condos Online, Condo Value Calculator, Toronto Condo Prices, How Much My Condo is Worth ' },
            { name: 'robots', content: 'index, follow' },
            { name: 'author', content: 'Surinder singh' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { name: 'date', content: '2020-09-27', scheme: 'YYYY-MM-DD' },
            { charset: 'UTF-8' }
        ]);
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Meta"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 58, vars: 3, consts: [[1, "test-card"], [1, "container", "cvc-container"], [1, "test-header"], [1, "test-title"], [1, "test-subtitle"], [1, "form-group", "form1"], [1, "cvc-form1"], ["ngx-google-places-autocomplete", "", "required", "", 1, "cvc-address", 3, "options", "onAddressChange"], ["placesRef", "ngx-places"], [1, "test-address"], ["id", "unit", "type", "number", "name", "unit", "placeholder", "Unit#", "required", "", 3, "formControl"], ["type", "submit", "mat-raised-button", "", "color", "primary", 1, "test-search", 3, "click"], [1, "container"], [1, "row"], [1, "col-md-12", "cvc-report"], [2, "font-weight", "500", "margin-top", "10px"], [1, "col-md-3"], ["src", "../../assets/images/samfarmaha.jpg", 1, "custom-image"], [1, "col-md-3", "cvc-realtor", 2, "margin-top", "30px"], ["routerLink", "/policy", "target", "_blank"], [1, "col-md-3", 2, "margin-top", "30px"], ["mat-card-image", "", "id", "cvc-century", "src", "assets/images/century21-1.png", "alt", "Photo of Century21"], [1, "col-md-3", "cvc-albinon", 2, "margin-top", "30px"], [2, "text-align", "center", "font-style", "italic", "margin-top", "10px"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "ATTENTION CONDO SELLERS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "YOUR CONDO MAY BE WORTH MORE THAN YOU THINK");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "BR");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "FIND OUT WHAT IS YOUR CONDO REALLY WORTH");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Just Fill The Home Evaluation Form Below");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Free service with no obligations");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "form", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "input", 7, 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("onAddressChange", function HomeComponent_Template_input_onAddressChange_15_listener($event) { return ctx.handleAddressChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_Template_button_click_22_listener() { return ctx.onButtonClick(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Click to proceed");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "h1", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Your Report will be prepared by a Realtor\u00AE. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " I may share your info with other Realtor\u00AE to help generate the report, only if request is generated outside of my Juristiction.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, " If you disagree or have an issue sharing your info with other Realtor\u00AE, then please do not fill the property evaluation form, sincere thanks for your visit. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "img", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Sam Farmaha Realtor\u00AE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, " Direct Line 416 951 2158.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Cell Phone 519 760 1456");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "a", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Privacy Policy | Terms of Service");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "img", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, " 1780 Albinon Road,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, " Unit #2 #3 TORONTO ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, " ONTARIO M9V1C1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "h3", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Thank you for vising my website I am here to provide my services and just a phone call away, you are under no obligation.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx.options);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Your place is: ", ctx.formattedAddress, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formControl", ctx.unit);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgForm"], ngx_google_places_autocomplete__WEBPACK_IMPORTED_MODULE_5__["GooglePlaceDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NumberValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlDirective"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_6__["NativeElementInjectorDirective"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_material_card__WEBPACK_IMPORTED_MODULE_8__["MatCardImage"]], styles: [".test-header[_ngcontent-%COMP%] {\n  margin: auto;\n  width: 70%;\n  padding: 1px;\n  text-align: center;\n  margin-top: 5%;\n}\n\n.cvc-report[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.test-content[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n}\n\n.cvc-form1[_ngcontent-%COMP%] {\n  margin: auto;\n  width: 50%;\n  border: none;\n  margin-top: 10%;\n  vertical-align: middle;\n}\n\n.cvc-container[_ngcontent-%COMP%] {\n  margin: auto;\n  position: relative;\n}\n\n#unit[_ngcontent-%COMP%] {\n  margin-top: -10px;\n}\n\n.test-address[_ngcontent-%COMP%] {\n  margin-left: 10px;\n}\n\n.test-search[_ngcontent-%COMP%] {\n  position: relative;\n  top: 50%;\n  left: 30%;\n  margin-top: 10px;\n  padding: 5px 2px;\n  font-size: 20px;\n  width: 40%;\n  background-color: #0081c9;\n}\n\n.test-card[_ngcontent-%COMP%] {\n  height: 50%;\n  background: url(\"https://cdn.pixabay.com/photo/2015/09/09/21/36/apartment-block-933538_1280.jpg\") no-repeat center center fixed;\n  background-size: cover;\n}\n\n.test-title[_ngcontent-%COMP%] {\n  font-size: 22px;\n  color: white;\n  font-weight: 500px;\n  text-align: center;\n  padding: 30px;\n  font-family: \"Poppins\";\n  margin-left: 20px;\n}\n\n.cvc-realtor[_ngcontent-%COMP%], .cvc-albinon[_ngcontent-%COMP%] {\n  font-size: 17px;\n  padding-top: 10px;\n  text-align: center;\n}\n\n.test-subtitle[_ngcontent-%COMP%] {\n  font-size: 25px;\n  color: white;\n  font-weight: 500px;\n  text-align: center;\n  padding: 10px;\n  font-family: \"Poppins\";\n}\n\n.cvc-address[_ngcontent-%COMP%] {\n  font-size: 25px;\n  color: black;\n  font-weight: 500px;\n  text-align: left;\n  padding: 10px;\n  font-family: \"Poppins\";\n  margin-left: 15px;\n}\n\n#cvc-century[_ngcontent-%COMP%], .custom-image[_ngcontent-%COMP%] {\n  width: 100%;\n  padding-top: 10px;\n  margin: auto;\n  display: block;\n}\n\n.child-1[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n#cvc-century[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.col-md-6[_ngcontent-%COMP%] {\n  text-align: center;\n  max-width: 100%;\n  margin-top: 10px;\n}\n\n\n\n@media screen and (min-width: 1200px) {\n  .custom-image[_ngcontent-%COMP%] {\n    width: 90%;\n    margin: auto;\n    display: block;\n  }\n\n  .test-title[_ngcontent-%COMP%] {\n    font-size: 22px;\n  }\n\n  .test-subtitle[_ngcontent-%COMP%] {\n    font-size: 20px;\n  }\n\n  .test-card[_ngcontent-%COMP%] {\n    height: 50%;\n  }\n\n  .cvc-form1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 50%;\n  }\n\n  .test-search[_ngcontent-%COMP%] {\n    position: relative;\n    top: 50%;\n    left: 30%;\n    margin-top: 10px;\n    padding: 2px 1px;\n    font-size: 20px;\n    width: 40%;\n  }\n}\n\n\n\n@media only screen and (max-width: 600px) {\n  .custom-image[_ngcontent-%COMP%] {\n    width: 70%;\n  }\n\n  .test-title[_ngcontent-%COMP%] {\n    font-size: 20px;\n    padding-bottom: 5px;\n    line-height: 1;\n  }\n\n  .test-subtitle[_ngcontent-%COMP%] {\n    font-size: 18px;\n  }\n\n  .cvc-address[_ngcontent-%COMP%] {\n    font-size: 25px;\n    color: black;\n    font-weight: 500px;\n    text-align: left;\n    padding: 10px;\n    font-family: \"Poppins\";\n    margin-left: 15px;\n  }\n\n  .test-card[_ngcontent-%COMP%] {\n    height: 70%;\n  }\n\n  .test-header[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 90%;\n    padding: 1px;\n    text-align: center;\n  }\n\n  .cvc-form1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 70%;\n  }\n\n  .test-search[_ngcontent-%COMP%] {\n    position: relative;\n    top: 50%;\n    left: 20%;\n    margin-top: 10px;\n    padding: 5px 2px;\n    font-size: 20px;\n    width: 60%;\n  }\n}\n\n\n\n@media only screen and (min-width: 600px) {\n  .custom-image[_ngcontent-%COMP%] {\n    width: 70%;\n  }\n\n  .test-title[_ngcontent-%COMP%] {\n    font-size: 21px;\n  }\n\n  .test-subtitle[_ngcontent-%COMP%] {\n    font-size: 15px;\n  }\n\n  .test-card[_ngcontent-%COMP%] {\n    height: 70%;\n  }\n\n  .cvc-form1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 50%;\n  }\n\n  .test-search[_ngcontent-%COMP%] {\n    position: relative;\n    top: 50%;\n    left: 20%;\n    margin-top: 10px;\n    padding: 2px 1px;\n    font-size: 20px;\n    width: 60%;\n  }\n}\n\n\n\n@media only screen and (min-width: 768px) {\n  .custom-image[_ngcontent-%COMP%] {\n    width: 70%;\n  }\n\n  .test-title[_ngcontent-%COMP%] {\n    font-size: 26px;\n  }\n\n  .test-subtitle[_ngcontent-%COMP%] {\n    font-size: 20px;\n  }\n\n  .test-card[_ngcontent-%COMP%] {\n    height: 70%;\n  }\n\n  .cvc-form1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 50%;\n  }\n\n  .test-search[_ngcontent-%COMP%] {\n    position: relative;\n    top: 50%;\n    left: 20%;\n    margin-top: 10px;\n    padding: 2px 1px;\n    font-size: 20px;\n    width: 60%;\n  }\n}\n\n\n\n@media only screen and (min-width: 992px) {\n  .custom-image[_ngcontent-%COMP%] {\n    width: 70%;\n  }\n\n  .test-title[_ngcontent-%COMP%] {\n    font-size: 25px;\n    line-height: 1;\n  }\n\n  .test-subtitle[_ngcontent-%COMP%] {\n    font-size: 20px;\n  }\n\n  .test-card[_ngcontent-%COMP%] {\n    height: 80%;\n  }\n\n  .cvc-form1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 50%;\n  }\n\n  .test-search[_ngcontent-%COMP%] {\n    position: relative;\n    top: 50%;\n    left: 30%;\n    margin-top: 10px;\n    padding: 5px 2px;\n    font-size: 20px;\n    width: 40%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUdBO0VBQ0csYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FBQUg7O0FBR0E7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUVELFlBQUE7RUFDQSxlQUFBO0VBQ0Esc0JBQUE7QUFESDs7QUFLQztFQUNJLFlBQUE7RUFDQSxrQkFBQTtBQUZMOztBQUtBO0VBQ0ksaUJBQUE7QUFGSjs7QUFNQTtFQUNJLGlCQUFBO0FBSEo7O0FBUUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7QUFMQTs7QUFRQTtFQUVJLFdBQUE7RUFFQSwrSEFBQTtFQUNBLHNCQUFBO0FBUEo7O0FBVUM7RUFDSSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtBQVBMOztBQVlDO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFUTDs7QUFZQztFQUNHLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQVRKOztBQWFDO0VBQ0csZUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBRUEsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7QUFYSjs7QUFpQkE7RUFDSSxXQUFBO0VBQ0QsaUJBQUE7RUFDQyxZQUFBO0VBQ0EsY0FBQTtBQWRKOztBQWtCQTtFQUNJLGtCQUFBO0FBZko7O0FBMkJFO0VBQ0ksV0FBQTtBQXhCTjs7QUE2QkE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQTFCSjs7QUE4QkEsb0VBQUE7O0FBQ0E7RUFDSTtJQUNBLFVBQUE7SUFDQSxZQUFBO0lBQ0EsY0FBQTtFQTNCRjs7RUE2QkU7SUFDSSxlQUFBO0VBMUJOOztFQTRCRTtJQUNJLGVBQUE7RUF6Qk47O0VBMkJFO0lBQ0ksV0FBQTtFQXhCTjs7RUEwQkU7SUFDRyxZQUFBO0lBQ0EsVUFBQTtFQXZCTDs7RUF5QkM7SUFDSSxrQkFBQTtJQUNBLFFBQUE7SUFDQSxTQUFBO0lBQ0EsZ0JBQUE7SUFDQSxnQkFBQTtJQUNBLGVBQUE7SUFDQSxVQUFBO0VBdEJMO0FBQ0Y7O0FBeUJJLGlEQUFBOztBQUNKO0VBQ0k7SUFDSSxVQUFBO0VBdkJOOztFQXlCTTtJQUNJLGVBQUE7SUFDQSxtQkFBQTtJQUNBLGNBQUE7RUF0QlY7O0VBeUJNO0lBQ0ksZUFBQTtFQXRCVjs7RUF3Qk07SUFDSSxlQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxhQUFBO0lBQ0Esc0JBQUE7SUFDQSxpQkFBQTtFQXJCVjs7RUF1Qk87SUFDSSxXQUFBO0VBcEJYOztFQXVCTztJQUNHLFlBQUE7SUFDQSxVQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0VBcEJWOztFQXVCTztJQUNHLFlBQUE7SUFDQSxVQUFBO0VBcEJWOztFQXNCTTtJQUNJLGtCQUFBO0lBQ0EsUUFBQTtJQUNBLFNBQUE7SUFDQSxnQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLFVBQUE7RUFuQlY7QUFDRjs7QUFzQkUsb0VBQUE7O0FBQ0E7RUFDRTtJQUNJLFVBQUE7RUFwQk47O0VBc0JNO0lBQ0ksZUFBQTtFQW5CVjs7RUFxQk07SUFDSSxlQUFBO0VBbEJWOztFQW9CTTtJQUNJLFdBQUE7RUFqQlY7O0VBbUJNO0lBQ0csWUFBQTtJQUNBLFVBQUE7RUFoQlQ7O0VBa0JLO0lBQ0ksa0JBQUE7SUFDQSxRQUFBO0lBQ0EsU0FBQTtJQUNBLGdCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxlQUFBO0lBQ0EsVUFBQTtFQWZUO0FBQ0Y7O0FBa0JFLHFEQUFBOztBQUNBO0VBQ0U7SUFDSSxVQUFBO0VBaEJOOztFQWtCTTtJQUNJLGVBQUE7RUFmVjs7RUFpQk07SUFDSSxlQUFBO0VBZFY7O0VBZ0JNO0lBQ0ksV0FBQTtFQWJWOztFQWVNO0lBQ0csWUFBQTtJQUNBLFVBQUE7RUFaVDs7RUFjSztJQUNJLGtCQUFBO0lBQ0EsUUFBQTtJQUNBLFNBQUE7SUFDQSxnQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLFVBQUE7RUFYVDtBQUNGOztBQWNFLG1EQUFBOztBQUNBO0VBQ0U7SUFDSSxVQUFBO0VBWk47O0VBY007SUFDSSxlQUFBO0lBQ0EsY0FBQTtFQVhWOztFQWFNO0lBQ0ksZUFBQTtFQVZWOztFQVlNO0lBQ0ksV0FBQTtFQVRWOztFQVdNO0lBQ0csWUFBQTtJQUNBLFVBQUE7RUFSVDs7RUFVSztJQUNJLGtCQUFBO0lBQ0EsUUFBQTtJQUNBLFNBQUE7SUFDQSxnQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLFVBQUE7RUFQVDtBQUNGIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRlc3QtaGVhZGVye1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgd2lkdGg6IDcwJTtcclxuICAgIHBhZGRpbmc6IDFweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IDUlO1xyXG59XHJcblxyXG4uY3ZjLXJlcG9ydHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuXHJcbi50ZXN0LWNvbnRlbnR7XHJcbiAgIGRpc3BsYXk6ZmxleDtcclxuICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgIHdpZHRoOjEwMCVcclxufVxyXG5cclxuLmN2Yy1mb3JtMXtcclxuICAgIG1hcmdpbjphdXRvIDtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgIC8vIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgIGJvcmRlcjogbm9uZTtcclxuICAgbWFyZ2luLXRvcDogMTAlO1xyXG4gICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG5cclxuXHJcbn1cclxuIC5jdmMtY29udGFpbmVye1xyXG4gICAgIG1hcmdpbjogYXV0bztcclxuICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiB9XHJcblxyXG4jdW5pdHtcclxuICAgIG1hcmdpbi10b3A6IC0xMHB4O1xyXG4gICAgLy9ib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgLy9ib3JkZXI6IG5vbmU7XHJcbn1cclxuLnRlc3QtYWRkcmVzc3tcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgLy9ib3JkZXI6IG5vbmU7XHJcbiAgICAvL2JvcmRlci1yYWRpdXM6IDMwcHg7XHJcbn1cclxuXHJcbi50ZXN0LXNlYXJjaHtcclxucG9zaXRpb246IHJlbGF0aXZlO1xyXG50b3A6IDUwJTtcclxubGVmdDogMzAlO1xyXG5tYXJnaW4tdG9wOiAxMHB4O1xyXG5wYWRkaW5nOiA1cHggMnB4O1xyXG5mb250LXNpemU6IDIwcHg7XHJcbndpZHRoOiA0MCU7XHJcbmJhY2tncm91bmQtY29sb3I6IFx0IzAwODFjOTtcclxufVxyXG5cclxuLnRlc3QtY2FyZCB7XHJcbiAgICBcclxuICAgIGhlaWdodDogNTAlO1xyXG4gICAgXHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoXCJodHRwczovL2Nkbi5waXhhYmF5LmNvbS9waG90by8yMDE1LzA5LzA5LzIxLzM2L2FwYXJ0bWVudC1ibG9jay05MzM1MzhfMTI4MC5qcGdcIiluby1yZXBlYXQgY2VudGVyIGNlbnRlciBmaXhlZDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiB9XHJcblxyXG4gLnRlc3QtdGl0bGV7XHJcbiAgICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICBmb250LXdlaWdodDo1MDBweDtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgcGFkZGluZzogMzBweDtcclxuICAgICBmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XHJcbiAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgICAvLyBtYXJnaW4tdG9wOiA1MHB4O1xyXG5cclxuIH1cclxuXHJcbiAuY3ZjLXJlYWx0b3IsIC5jdmMtYWxiaW5vbntcclxuICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbiAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gfVxyXG5cclxuIC50ZXN0LXN1YnRpdGxle1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6NTAwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IFwiUG9wcGluc1wiO1xyXG4gfVxyXG5cclxuIFxyXG4gLmN2Yy1hZGRyZXNze1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgY29sb3I6YmxhY2s7XHJcbiAgICBmb250LXdlaWdodDo1MDBweDtcclxuICAgIHRleHQtYWxpZ246bGVmdDtcclxuICAgIC8vYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBmb250LWZhbWlseTogXCJQb3BwaW5zXCI7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxuICAgIC8vYm9yZGVyOiBub25lO1xyXG4gfVxyXG5cclxuXHJcblxyXG4jY3ZjLWNlbnR1cnksIC5jdXN0b20taW1hZ2V7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgLy9tYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuLmNoaWxkLTF7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG4vLyAuZmxleC1jb250YWluZXJ7XHJcbi8vICAgICBiYWNrZ3JvdW5kLWNvbG9yOndoaXRlc21va2U7XHJcbi8vICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbi8vICAgICBwYWRkaW5nOjFweDtcclxuLy8gICAgIHdpZHRoOiAxMDAlO1xyXG4vLyAgIH1cclxuICBcclxuICBcclxuICAjY3ZjLWNlbnR1cnl7XHJcbiAgICAgIHdpZHRoOjEwMCU7XHJcbiAgfVxyXG5cclxuXHJcblxyXG4uY29sLW1kLTZ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG5cclxufVxyXG5cclxuLyogRXh0cmEgbGFyZ2UgZGV2aWNlcyAobGFyZ2UgbGFwdG9wcyBhbmQgZGVza3RvcHMsIDEyMDBweCBhbmQgdXApICovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6MTIwMHB4KXtcclxuICAgIC5jdXN0b20taW1hZ2V7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbiAgICAudGVzdC10aXRsZXtcclxuICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICB9XHJcbiAgICAudGVzdC1zdWJ0aXRsZXtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICB9XHJcbiAgICAudGVzdC1jYXJke1xyXG4gICAgICAgIGhlaWdodDogNTAlO1xyXG4gICAgfVxyXG4gICAgLmN2Yy1mb3JtMXtcclxuICAgICAgIG1hcmdpbjphdXRvIDtcclxuICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgIH1cclxuICAgLnRlc3Qtc2VhcmNoe1xyXG4gICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICBsZWZ0OiAzMCU7XHJcbiAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgcGFkZGluZzogMnB4IDFweDtcclxuICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgIHdpZHRoOiA0MCU7XHJcbiAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyogRXh0cmEgc21hbGwgZGV2aWNlcyAocGhvbmVzLCA2MDBweCBhbmQgZG93bikgKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gICAgLmN1c3RvbS1pbWFnZXtcclxuICAgICAgICB3aWR0aDogNzAlXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LXRpdGxle1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XHJcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxLjA7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICAudGVzdC1zdWJ0aXRsZXtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY3ZjLWFkZHJlc3N7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgICAgICAgICAgY29sb3I6YmxhY2s7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OjUwMHB4O1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOmxlZnQ7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIlBvcHBpbnNcIjtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICAgICAgIH1cclxuICAgICAgICAgLnRlc3QtY2FyZHtcclxuICAgICAgICAgICAgIGhlaWdodDogNzAlO1xyXG5cclxuICAgICAgICAgfVxyXG4gICAgICAgICAudGVzdC1oZWFkZXJ7XHJcbiAgICAgICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICAgICAgcGFkZGluZzogMXB4O1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgICAuY3ZjLWZvcm0xe1xyXG4gICAgICAgICAgICBtYXJnaW46YXV0byA7XHJcbiAgICAgICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LXNlYXJjaHtcclxuICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICAgbGVmdDogMjAlO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nOiA1cHggMnB4O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiA2MCU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgLyogU21hbGwgZGV2aWNlcyAocG9ydHJhaXQgdGFibGV0cyBhbmQgbGFyZ2UgcGhvbmVzLCA2MDBweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xyXG4gICAgLmN1c3RvbS1pbWFnZXtcclxuICAgICAgICB3aWR0aDogNzAlXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LXRpdGxle1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIxcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LXN1YnRpdGxle1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LWNhcmR7XHJcbiAgICAgICAgICAgIGhlaWdodDogNzAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY3ZjLWZvcm0xe1xyXG4gICAgICAgICAgIG1hcmdpbjphdXRvIDtcclxuICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgfVxyXG4gICAgICAgLnRlc3Qtc2VhcmNoe1xyXG4gICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICBsZWZ0OiAyMCU7XHJcbiAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICAgICBwYWRkaW5nOiAycHggMXB4O1xyXG4gICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICB3aWR0aDogNjAlO1xyXG4gICAgICAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgLyogTWVkaXVtIGRldmljZXMgKGxhbmRzY2FwZSB0YWJsZXRzLCA3NjhweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkge1xyXG4gICAgLmN1c3RvbS1pbWFnZXtcclxuICAgICAgICB3aWR0aDogNzAlXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LXRpdGxle1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDI2cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LXN1YnRpdGxle1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC50ZXN0LWNhcmR7XHJcbiAgICAgICAgICAgIGhlaWdodDogNzAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuY3ZjLWZvcm0xe1xyXG4gICAgICAgICAgIG1hcmdpbjphdXRvIDtcclxuICAgICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgfVxyXG4gICAgICAgLnRlc3Qtc2VhcmNoe1xyXG4gICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICAgICB0b3A6IDUwJTtcclxuICAgICAgICAgICBsZWZ0OiAyMCU7XHJcbiAgICAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICAgICBwYWRkaW5nOiAycHggMXB4O1xyXG4gICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICAgICB3aWR0aDogNjAlO1xyXG4gICAgICAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgLyogTGFyZ2UgZGV2aWNlcyAobGFwdG9wcy9kZXNrdG9wcywgOTkycHggYW5kIHVwKSAqL1xyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogOTkycHgpIHtcclxuICAgIC5jdXN0b20taW1hZ2V7XHJcbiAgICAgICAgd2lkdGg6IDcwJVxyXG4gICAgICAgIH1cclxuICAgICAgICAudGVzdC10aXRsZXtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMS4wO1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGVzdC1zdWJ0aXRsZXtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAudGVzdC1jYXJke1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDgwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLmN2Yy1mb3JtMXtcclxuICAgICAgICAgICBtYXJnaW46YXV0byA7XHJcbiAgICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgIH1cclxuICAgICAgIC50ZXN0LXNlYXJjaHtcclxuICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgICAgdG9wOiA1MCU7XHJcbiAgICAgICAgICAgbGVmdDogMzAlO1xyXG4gICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgICAgcGFkZGluZzogNXB4IDJweDtcclxuICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgd2lkdGg6IDQwJTtcclxuICAgICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.scss']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["Meta"] }]; }, null); })();


/***/ }),

/***/ "./src/app/policy/policy.component.ts":
/*!********************************************!*\
  !*** ./src/app/policy/policy.component.ts ***!
  \********************************************/
/*! exports provided: PolicyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolicyComponent", function() { return PolicyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");





class PolicyComponent {
    constructor() { }
    ngOnInit() {
    }
}
PolicyComponent.ɵfac = function PolicyComponent_Factory(t) { return new (t || PolicyComponent)(); };
PolicyComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: PolicyComponent, selectors: [["app-policy"]], decls: 33, vars: 0, consts: [[1, "container"], [1, "row", 2, "margin-top", "5px"], ["mat-card-image", "", "id", "cvc-policy", "src", "assets/images/policy.png"], [1, "row"], [1, "col", "heading", 2, "text-align", "center", "color", "darkblue"], [1, "col-md-12", "cvc-report"], [2, "font-weight", "400", "text-align", "center"], [1, "fas", "fa-angle-double-right"], [1, "row", 2, "text-align", "center"], [1, "col"], ["mat-button", "", "routerLink", "/", 2, "font", "size 30px"]], template: function PolicyComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Disclaimer, Privacy Policy and Prohibited Use of the Site and Content.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Your Report will be prepared by a Realtor\u00AE. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "\u00A0I may seek your info with other Realtor\u00AE to help generate the report, if request is generated outside of my Juristiction.If you disagree or have an issue sharing your info with other Realtor\u00AE, then please do not fill the property evaluation form, sincere thanks for your visit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "\u00A0*Not intended to solicit Buyers or Sellers already under contract, by using the Site and its mobile versions, and by subscribing to email services you agree to comply with the Agreement and confirm that you have a bona fide interest in buying, selling or leasing a residential property.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "\u00A0If you do not agree to comply with the Agreement, you may not use the Site or subscribe to email services. You are prohibited Use of the Site and Content to Invade another person\u2019s privacy or collect, disclose or store personal information about other persons, including telephone numbers, street addresses, email addresses, last names, etc. The materials on this website are provided \u201Cas is\u201D. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " \u00A0We do not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on this website or on any websites linked to this site. We have no control over the authenticity of the content on display. In this website Realtor word is used to describe a sales Representative, REALTOR\u00AE logo are certification marks owned by REALTOR\u00AE Canada Inc., a corporation jointly owned by the National Association of REALTORS\u00AE and CREA.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, " \u00A0The REALTOR\u00AE trademarks are used to identify real estate services provided by brokers and salespersons who are members of CREA and who accept and respect a strict Code of Ethics, and are required to meet consistent professional standards of business practice which is the consumer\u2019s assurance of integrity. These Terms of Use shall be governed by the laws of Ontario, Canada.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "\u00A0All users of this website are bound by these conditions and should therefore periodically visit this page to review any changes to these requirements. The users continued use of this website following the posting of any changes constitutes acceptance by the user of such modifications. Sincere Thanks for your visit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Back to main page");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_1__["MatCardImage"], _angular_material_button__WEBPACK_IMPORTED_MODULE_2__["MatAnchor"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]], styles: [".heading[_ngcontent-%COMP%] {\n  font-size: 30px;\n  font-weight: 700;\n  font-family: Arial, Helvetica, sans-serif;\n  margin-top: 5%;\n}\n\n.content[_ngcontent-%COMP%] {\n  font-size: 14px;\n}\n\n#cvc-policy[_ngcontent-%COMP%] {\n  width: 100%;\n  display: block;\n}\n\n.fa-angle-double-right[_ngcontent-%COMP%] {\n  font-size: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcG9saWN5L3BvbGljeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlDQUFBO0VBQ0EsY0FBQTtBQURKOztBQUtBO0VBQ0ksZUFBQTtBQUZKOztBQUtBO0VBQ0ksV0FBQTtFQUNBLGNBQUE7QUFGSjs7QUFLQTtFQUNJLGVBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL3BvbGljeS9wb2xpY3kuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbi5oZWFkaW5ne1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG4gICAgbWFyZ2luLXRvcDogNSU7XHJcbn1cclxuXHJcblxyXG4uY29udGVudHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5cclxuI2N2Yy1wb2xpY3l7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uZmEtYW5nbGUtZG91YmxlLXJpZ2h0e1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PolicyComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-policy',
                templateUrl: './policy.component.html',
                styleUrls: ['./policy.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/submit1/submit1.component.ts":
/*!**********************************************!*\
  !*** ./src/app/submit1/submit1.component.ts ***!
  \**********************************************/
/*! exports provided: Submit1Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Submit1Component", function() { return Submit1Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/__ivy_ngcc__/fesm2015/agm-core.js");
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout/flex */ "./node_modules/@angular/flex-layout/__ivy_ngcc__/esm2015/flex.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");










class Submit1Component {
    //mapType = 'satellite';
    constructor(authService, titleService, metaTagService) {
        this.authService = authService;
        this.titleService = titleService;
        this.metaTagService = metaTagService;
        this.title = 'Condo Value Check Submit 1';
        this.zoom = 20;
        this.sat = 'hybrid';
    }
    ngOnInit() {
        this.authService.currentMessage.subscribe(data => {
            this.lat = data['lat'];
            this.lng = data['long'];
        });
        this.titleService.setTitle(this.title);
        this.metaTagService.updateTag({ name: 'description', content: 'Add song template' });
    }
}
Submit1Component.ɵfac = function Submit1Component_Factory(t) { return new (t || Submit1Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Meta"])); };
Submit1Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Submit1Component, selectors: [["app-submit1"]], decls: 42, vars: 6, consts: [[1, "cvc-card"], [1, "cvc-header"], [1, "cvc-title"], [1, "container-fluid"], [1, "cvc-content"], [1, "cvc-form"], [3, "latitude", "longitude", "zoom", "mapTypeId"], [3, "latitude", "longitude"], ["fxLayout", "row", "rowHeight", "1:1", 1, "flex-container"], ["fxFlex", "40", 1, "child-10"], ["id", "cvc-search", "mat-raised-button", "", "routerLink", "/submit2", 2, "background-color", "#0081c9", "color", "white"], ["fxFlex", "40", 1, "child-20"], ["id", "cvc-search", "mat-raised-button", "", "color", "warn", "routerLink", "/"], [1, "container"], [1, "row"], [1, "col-md-3", "cvc-realtor"], ["routerLink", "/policy", "target", "_blank"], [1, "col-md-6"], ["mat-card-image", "", "id", "cvc-century", "src", "assets/images/century21-1.png", "alt", "Photo of Century21"], [1, "col-md-3", "cvc-albinon"]], template: function Submit1Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card-header", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-card-title", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Nice place!! Is this place correct? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-card-content", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "form", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "agm-map", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "agm-marker", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Yes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "No");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "\u00A0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Your Report will be");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " prepared by a Realtor\u00AE.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Sam Farmaha Realtor\u00AE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, " Direct Line 416 951 2158.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Privacy Policy | Terms of Service");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "img", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, " 1780 Albinon Road,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, " Unit #3 ON M9V 1C1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Call: 416-742-8000");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Fax: 416-742-8001");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("latitude", ctx.lat)("longitude", ctx.lng)("zoom", ctx.zoom)("mapTypeId", ctx.sat);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("latitude", ctx.lat)("longitude", ctx.lng);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardHeader"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardTitle"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"], _agm_core__WEBPACK_IMPORTED_MODULE_5__["AgmMap"], _agm_core__WEBPACK_IMPORTED_MODULE_5__["AgmMarker"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__["DefaultLayoutDirective"], _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_6__["DefaultFlexDirective"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterLink"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["RouterLinkWithHref"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardImage"]], styles: ["agm-map[_ngcontent-%COMP%] {\n  height: 500px;\n  width: 100%;\n  justify-content: center;\n}\n\n.cvc-card[_ngcontent-%COMP%] {\n  height: 100%;\n  background-color: lightgray;\n  background-size: cover;\n  width: 100%;\n}\n\n.cvc-header[_ngcontent-%COMP%] {\n  height: 10%;\n}\n\n.cvc-title[_ngcontent-%COMP%] {\n  margin: auto;\n  width: 100%;\n  padding: 1px;\n  font-size: 35px;\n  font-weight: 600px;\n  padding-top: 10px;\n  color: black;\n}\n\n.cvc-content[_ngcontent-%COMP%], .cvc-header[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n}\n\n#mySelectId[_ngcontent-%COMP%] {\n  width: 50%;\n  font-size: 18px;\n}\n\n#cvc-search[_ngcontent-%COMP%] {\n  width: 30%;\n  position: relative;\n  margin-top: 10px;\n  padding: 2px 2px;\n  font-size: 20px;\n}\n\n.test-col[_ngcontent-%COMP%] {\n  font-size: 15px;\n  color: black;\n}\n\n#cvc-century[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-top: 10px;\n}\n\n.col-md-6[_ngcontent-%COMP%] {\n  text-align: center;\n  max-width: 100%;\n}\n\n.cvc-report[_ngcontent-%COMP%] {\n  padding-top: 1px;\n}\n\n.cvc-realtor[_ngcontent-%COMP%], .cvc-albinon[_ngcontent-%COMP%] {\n  font-size: 17px;\n  padding-top: 10px;\n  text-align: center;\n}\n\n#cvc-century[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-top: 10px;\n  margin: auto;\n  display: block;\n}\n\n\n\n@media screen and (min-width: 1200px) {\n  .col-md-6[_ngcontent-%COMP%] {\n    text-align: center;\n    max-width: 100%;\n    margin-top: 5px;\n  }\n\n  .cvc-form[_ngcontent-%COMP%] {\n    width: 80%;\n  }\n}\n\n\n\n\n\n\n\n@media only screen and (min-width: 600px) {\n  .col-md-6[_ngcontent-%COMP%] {\n    text-align: center;\n    max-width: 100%;\n    margin-top: 5px;\n  }\n}\n\n\n\n@media only screen and (max-width: 600px) {\n  .col-md-6[_ngcontent-%COMP%] {\n    text-align: center;\n    max-width: 100%;\n    margin-top: 10px;\n  }\n\n  .cvc-title[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    padding: 1px;\n    font-size: 25px;\n    font-weight: 600px;\n    padding-top: 10px;\n    color: whitesmoke;\n  }\n\n  .cvc-realtor[_ngcontent-%COMP%] {\n    font-size: 18px;\n    padding-top: 10px;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 50%;\n    padding-top: 20px;\n  }\n\n  .cvc-report[_ngcontent-%COMP%] {\n    padding-top: 10px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VibWl0MS9zdWJtaXQxLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUVFO0VBRUUsWUFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0FBQUo7O0FBSUM7RUFDSSxXQUFBO0FBREw7O0FBR0M7RUFDRyxZQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUFBSjs7QUFHQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQUFKOztBQUtBO0VBQ0ksVUFBQTtFQUNBLGVBQUE7QUFGSjs7QUFLQTtFQUNJLFVBQUE7RUFDQSxrQkFBQTtFQUVKLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBSEE7O0FBUUE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQUxKOztBQVNBO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0FBTko7O0FBU0E7RUFDSSxrQkFBQTtFQUNBLGVBQUE7QUFOSjs7QUFVQTtFQUNJLGdCQUFBO0FBUEo7O0FBVUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQVBKOztBQVVBO0VBQ0ksVUFBQTtFQUNELGlCQUFBO0VBQ0MsWUFBQTtFQUNBLGNBQUE7QUFQSjs7QUFZQSxvRUFBQTs7QUFDQTtFQUVJO0lBQ0ksa0JBQUE7SUFDQSxlQUFBO0lBQ0EsZUFBQTtFQVZOOztFQWNDO0lBQ0ksVUFBQTtFQVhMO0FBQ0Y7O0FBaUJFLG1EQUFBOztBQU1FLHFEQUFBOztBQU1DLG9FQUFBOztBQUNIO0VBRUU7SUFDSSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxlQUFBO0VBMUJOO0FBQ0Y7O0FBa0NNLGlEQUFBOztBQUNOO0VBR0k7SUFDSSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtFQWxDTjs7RUFzQ0U7SUFDSSxZQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0lBQ0Esa0JBQUE7SUFDQSxpQkFBQTtJQUNBLGlCQUFBO0VBbkNOOztFQXNDRTtJQUNJLGVBQUE7SUFDQSxpQkFBQTtFQW5DTjs7RUF1Q0U7SUFDSSxVQUFBO0lBQ0EsaUJBQUE7RUFwQ047O0VBdUNFO0lBQ0ksaUJBQUE7RUFwQ047QUFDRiIsImZpbGUiOiJzcmMvYXBwL3N1Ym1pdDEvc3VibWl0MS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImFnbS1tYXAge1xyXG4gICAgaGVpZ2h0OiA1MDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICAuY3ZjLWNhcmQge1xyXG4gICAgXHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyYXk7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgXHJcbiB9XHJcblxyXG4gLmN2Yy1oZWFkZXJ7XHJcbiAgICAgaGVpZ2h0OiAxMCU7XHJcbiB9XHJcbiAuY3ZjLXRpdGxle1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbiAgICBmb250LXNpemU6IDM1cHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDoxMHB4IDtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuLmN2Yy1jb250ZW50LCAuY3ZjLWhlYWRlcntcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcblxyXG59XHJcblxyXG5cclxuI215U2VsZWN0SWR7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG59XHJcblxyXG4jY3ZjLXNlYXJjaHtcclxuICAgIHdpZHRoOiAzMCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG5tYXJnaW4tdG9wOiAxMHB4O1xyXG5wYWRkaW5nOiAycHggMnB4O1xyXG5mb250LXNpemU6IDIwcHg7XHJcblxyXG59XHJcblxyXG5cclxuLnRlc3QtY29se1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgY29sb3I6YmxhY2s7XHJcbn1cclxuXHJcblxyXG4jY3ZjLWNlbnR1cnl7XHJcbiAgICB3aWR0aDo1MCU7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG5cclxuLmNvbC1tZC02e1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG5cclxufVxyXG5cclxuLmN2Yy1yZXBvcnR7XHJcbiAgICBwYWRkaW5nLXRvcDogMXB4O1xyXG59XHJcblxyXG4uY3ZjLXJlYWx0b3IsIC5jdmMtYWxiaW5vbntcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4jY3ZjLWNlbnR1cnl7XHJcbiAgICB3aWR0aDo1MCU7XHJcbiAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAvL21hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG5cclxuLyogRXh0cmEgbGFyZ2UgZGV2aWNlcyAobGFyZ2UgbGFwdG9wcyBhbmQgZGVza3RvcHMsIDEyMDBweCBhbmQgdXApICovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6MTIwMHB4KXtcclxuICAgXHJcbiAgICAuY29sLW1kLTZ7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICBcclxuICAgIH0gIFxyXG5cclxuICAgLmN2Yy1mb3Jte1xyXG4gICAgICAgd2lkdGg6IDgwJTtcclxuXHJcbiAgICB9XHJcblxyXG5cclxufVxyXG5cclxuICAvKiBMYXJnZSBkZXZpY2VzIChsYXB0b3BzL2Rlc2t0b3BzLCA5OTJweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTJweCkge1xyXG5cclxuICB9XHJcblxyXG5cclxuICAgIC8qIE1lZGl1bSBkZXZpY2VzIChsYW5kc2NhcGUgdGFibGV0cywgNzY4cHggYW5kIHVwKSAqL1xyXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkge1xyXG5cclxuICAgIH1cclxuXHJcblxyXG4gICAgIC8qIFNtYWxsIGRldmljZXMgKHBvcnRyYWl0IHRhYmxldHMgYW5kIGxhcmdlIHBob25lcywgNjAwcHggYW5kIHVwKSAqL1xyXG4gIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNjAwcHgpIHtcclxuICAgXHJcbiAgICAuY29sLW1kLTZ7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1heC13aWR0aDogMTAwJTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICBcclxuICAgIH1cclxuXHJcblxyXG5cclxuXHJcbiAgfVxyXG5cclxuICAgICAgLyogRXh0cmEgc21hbGwgZGV2aWNlcyAocGhvbmVzLCA2MDBweCBhbmQgZG93bikgKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG5cclxuICBcclxuICAgIC5jb2wtbWQtNntcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBcclxuICAgIH1cclxuXHJcbiAgICAuY3ZjLXRpdGxle1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiAxcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcclxuICAgICAgICBwYWRkaW5nLXRvcDoxMHB4IDtcclxuICAgICAgICBjb2xvcjogd2hpdGVzbW9rZTtcclxuICAgIH1cclxuXHJcbiAgICAuY3ZjLXJlYWx0b3J7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgfVxyXG4gICBcclxuXHJcbiAgICAjY3ZjLWNlbnR1cnl7XHJcbiAgICAgICAgd2lkdGg6NTAlO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5jdmMtcmVwb3J0e1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuXHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Submit1Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-submit1',
                templateUrl: './submit1.component.html',
                styleUrls: ['./submit1.component.scss']
            }]
    }], function () { return [{ type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Meta"] }]; }, null); })();


/***/ }),

/***/ "./src/app/submit2/submit2.component.ts":
/*!**********************************************!*\
  !*** ./src/app/submit2/submit2.component.ts ***!
  \**********************************************/
/*! exports provided: Submit2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Submit2Component", function() { return Submit2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-intl-tel-input */ "./node_modules/ngx-intl-tel-input/__ivy_ngcc__/fesm2015/ngx-intl-tel-input.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");












function Submit2Component_mat_option_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bedrooms_r8 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", bedrooms_r8.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", bedrooms_r8.name, " ");
} }
function Submit2Component_mat_error_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-error");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Please choose a value");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function Submit2Component_mat_option_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const bathrooms_r9 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", bathrooms_r9.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", bathrooms_r9.name, " ");
} }
function Submit2Component_mat_error_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-error");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Please choose a value");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function Submit2Component_mat_option_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const squarefeet_r10 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", squarefeet_r10.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", squarefeet_r10.name, " ");
} }
function Submit2Component_mat_error_31_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-error");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Please choose a value");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function Submit2Component_mat_option_36_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-option", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const sellingin_r11 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", sellingin_r11.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", sellingin_r11.name, " ");
} }
function Submit2Component_mat_error_37_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-error");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Please choose a value");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
class Submit2Component {
    constructor(router, formBuilder) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.bedroomsControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.bedroomss = [
            { name: '1' },
            { name: '2' },
            { name: '3' },
            { name: '4' },
            { name: '5' },
            { name: 'more than 5' }
        ];
        this.bathroomsControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.bathroomss = [
            { name: '1' },
            { name: '2' },
            { name: '3' },
            { name: '4' },
            { name: '5' },
            { name: 'more than 5' }
        ];
        this.squarefeetControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.squarefeets = [
            { name: '0-200' },
            { name: '200-400' },
            { name: '400-600' },
            { name: '600-800' },
            { name: '800-1000' },
            { name: '1000-1200' },
            { name: '1200-1400' },
            { name: '1400-1600' },
            { name: '1600-1800' },
            { name: '1800-2000' },
            { name: '2000-2200' },
            { name: '2200-2400' },
            { name: 'more than 2400' }
        ];
        this.sellinginControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.sellingins = [
            { name: '1-3 months' },
            { name: '3-6 months' },
            { name: '6-12 months' },
            { name: 'just curious' },
            { name: 'refinancing' },
        ];
    }
    ngOnInit() {
        this.form = this.formBuilder.group({
            bedroomsControl: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            bathroomsControl: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            squarefeetControl: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            sellinginControl: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    }
    onFirstSubmit() {
        //console.log(this.form.value)
        const data1 = {
            "bedrooms": this.form.value.bedroomsControl,
            "sell": this.form.value.sellinginControl,
            "bath": this.form.value.bathroomsControl,
            "sq": this.form.value.squarefeetControl
        };
        //console.log(data1);
        localStorage.setItem('data1', JSON.stringify(data1));
        this.router.navigate(['/submit3']);
    }
}
Submit2Component.ɵfac = function Submit2Component_Factory(t) { return new (t || Submit2Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"])); };
Submit2Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Submit2Component, selectors: [["app-submit2"]], decls: 69, vars: 10, consts: [[1, "cvc-card"], [1, "cvc-header"], [1, "cvc-title"], [1, "cvc-content"], ["role", "form", 1, "cvc-form", 3, "formGroup"], ["appearance", "fill", 1, "cvc-formfield"], ["placeholder", "No of bedrooms", "formControlName", "bedroomsControl", "required", ""], ["required", "", 3, "value", 4, "ngFor", "ngForOf"], [4, "ngIf"], ["placeholder", "No of bathrooms", "formControlName", "bathroomsControl", "required", ""], [3, "value", 4, "ngFor", "ngForOf"], ["placeholder", "No of squarefeet", "formControlName", "squarefeetControl", "required", ""], ["placeholder", "Selling in", "formControlName", "sellinginControl", "required", ""], ["type", "submit", "id", "cvc-search", "mat-raised-button", "", "color", "primary", 3, "disabled", "click"], [1, "container"], [1, "row"], [1, "col-md-3", "cvc-realtor"], ["routerLink", "/policy", "target", "_blank"], [1, "col-md-6"], ["mat-card-image", "", "id", "cvc-century", "src", "assets/images/century21-1.png", "alt", "Photo of Century21"], [1, "col-md-3", "cvc-albinon"], ["required", "", 3, "value"], [3, "value"]], template: function Submit2Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card-header", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-card-title", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Nice place!! Please verify the details");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-card-content", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-form-field", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "No of bedrooms");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-select", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-option");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, Submit2Component_mat_option_12_Template, 2, 2, "mat-option", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, Submit2Component_mat_error_13_Template, 2, 0, "mat-error", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-form-field", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "No of bathrooms");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-select", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "mat-option");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, Submit2Component_mat_option_21_Template, 2, 2, "mat-option", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, Submit2Component_mat_error_22_Template, 2, 0, "mat-error", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "mat-form-field", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Squarefeet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-select", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "mat-option");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](30, Submit2Component_mat_option_30_Template, 2, 2, "mat-option", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, Submit2Component_mat_error_31_Template, 2, 0, "mat-error", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "mat-form-field", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "mat-select", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-option");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "--");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](36, Submit2Component_mat_option_36_Template, 2, 2, "mat-option", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](37, Submit2Component_mat_error_37_Template, 2, 0, "mat-error", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](38, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Submit2Component_Template_button_click_39_listener() { return ctx.onFirstSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Calculate my value");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "\u00A0\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Your Report will be");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, " prepared by a Realtor\u00AE.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Sam Farmaha Realtor\u00AE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, " Direct Line 416 951 2158.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Privacy Policy | Terms of Service");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, " 1780 Albinon Road,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, " Unit #3 ON M9V 1C1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Call: 416-742-8000");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "Fax: 416-742-8001");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.bedroomss);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.bedroomsControl.hasError("required"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.bathroomss);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.bathroomsControl.hasError("required"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.squarefeets);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.squarefeetControl.hasError("required"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.sellingins);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.sellinginControl.hasError("required"));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardHeader"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardTitle"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatLabel"], _angular_material_select__WEBPACK_IMPORTED_MODULE_5__["MatSelect"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_6__["NativeElementInjectorDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_material_core__WEBPACK_IMPORTED_MODULE_7__["MatOption"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_material_button__WEBPACK_IMPORTED_MODULE_9__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardImage"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatError"]], styles: [".cvc-card[_ngcontent-%COMP%] {\n  height: 100%;\n  background-color: violet;\n  background: url(\"https://cdn.pixabay.com/photo/2015/03/26/09/41/condominium-690086_1280.jpg\") no-repeat center center fixed;\n  background-size: cover;\n  width: 100%;\n  margin-left: 0px;\n}\n\n.cvc-realtor[_ngcontent-%COMP%] {\n  font-size: 18px;\n  padding-top: 10px;\n}\n\n.cvc-header[_ngcontent-%COMP%] {\n  height: 10%;\n}\n\n.cvc-title[_ngcontent-%COMP%] {\n  margin: auto;\n  width: 100%;\n  padding: 10px;\n  font-size: 30px;\n  font-weight: 600px;\n  padding-top: 10px;\n  color: whitesmoke;\n}\n\n.cvc-content[_ngcontent-%COMP%], .cvc-header[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n}\n\n.cvc-formfield[_ngcontent-%COMP%] {\n  width: 100%;\n  font-size: 18px;\n  align-content: center;\n  color: black;\n  padding: 5px;\n}\n\n#mySelectId[_ngcontent-%COMP%] {\n  width: 100%;\n  font-size: 18px;\n}\n\n#cvc-search[_ngcontent-%COMP%] {\n  width: 50%;\n  background-color: #0081c9;\n  position: relative;\n  left: 20%;\n  padding: 5px 2px;\n  font-size: 20px;\n}\n\n.cvc-checkbox[_ngcontent-%COMP%] {\n  font-size: 13px;\n}\n\n.test-col[_ngcontent-%COMP%] {\n  font-size: 15px;\n  color: black;\n}\n\n.cvc-form[_ngcontent-%COMP%] {\n  background-color: snow;\n  width: 50% !important;\n  padding: 10px;\n  height: auto;\n}\n\n.cvc-formnameemail[_ngcontent-%COMP%] {\n  -webkit-text-decoration-color: black;\n          text-decoration-color: black;\n}\n\n#cvc-century[_ngcontent-%COMP%] {\n  width: 50%;\n}\n\n.col-md-6[_ngcontent-%COMP%] {\n  text-align: center;\n  margin-top: 10px;\n}\n\n.container[_ngcontent-%COMP%] {\n  height: 50px;\n}\n\n.cvc-checkbox[_ngcontent-%COMP%] {\n  width: 50%;\n}\n\n.cvc-realtor[_ngcontent-%COMP%], .cvc-albinon[_ngcontent-%COMP%] {\n  font-size: 17px;\n  padding-top: 10px;\n  text-align: center;\n}\n\n#cvc-century[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-top: 10px;\n  margin: auto;\n  display: block;\n}\n\n\n\n\n\n\n\n@media only screen and (min-width: 768px) {\n  .cvc-card[_ngcontent-%COMP%] {\n    height: 100%;\n    background-color: violet;\n    background: url(\"https://cdn.pixabay.com/photo/2015/03/26/09/41/condominium-690086_1280.jpg\") no-repeat center center fixed;\n    background-size: cover;\n    width: 100%;\n    margin-left: 0px;\n  }\n\n  .cvc-title[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    padding: 10px;\n    font-size: 30px;\n    font-weight: 600px;\n    padding-top: 30px;\n    color: whitesmoke;\n  }\n\n  .cvc-form[_ngcontent-%COMP%] {\n    background-color: snow;\n    width: 50% !important;\n    padding: 10px;\n    height: 40%;\n  }\n}\n\n\n\n\n\n@media only screen and (max-width: 600px) {\n  .cvc-card[_ngcontent-%COMP%] {\n    height: 100%;\n    background-color: violet;\n    background: url(\"https://cdn.pixabay.com/photo/2015/03/26/09/41/condominium-690086_1280.jpg\") no-repeat center center fixed;\n    background-size: cover;\n    width: 100%;\n  }\n\n  .cvc-title[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    padding: 10px;\n    font-size: 22px;\n    font-weight: 600px;\n    padding-top: 30px;\n    color: whitesmoke;\n  }\n\n  .cvc-form[_ngcontent-%COMP%] {\n    background-color: snow;\n    width: 80% !important;\n    padding: 10px;\n    height: 40%;\n  }\n\n  #cvc-search[_ngcontent-%COMP%] {\n    width: 50%;\n    position: relative;\n    left: 20%;\n    padding: 5px 2px;\n    font-size: 20px;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 50%;\n    margin-top: 10px;\n  }\n\n  .cvc-checkbox[_ngcontent-%COMP%] {\n    width: 20%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VibWl0Mi9zdWJtaXQyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRUksWUFBQTtFQUNBLHdCQUFBO0VBQ0EsMkhBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQUFKOztBQUVDO0VBQ0csZUFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUM7RUFDSSxXQUFBO0FBQ0w7O0FBQ0M7RUFDRyxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FBRUo7O0FBQ0E7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUFFSjs7QUFDQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQUVKOztBQUNBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7QUFFSjs7QUFDQTtFQUNJLFVBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBRUosU0FBQTtFQUVBLGdCQUFBO0VBQ0EsZUFBQTtBQUFBOztBQUlBO0VBQ0ksZUFBQTtBQURKOztBQUlBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUFESjs7QUFLQTtFQUNJLHNCQUFBO0VBQ0MscUJBQUE7RUFDRCxhQUFBO0VBQ0EsWUFBQTtBQUZKOztBQUtBO0VBQ0ksb0NBQUE7VUFBQSw0QkFBQTtBQUZKOztBQUtBO0VBQ0ksVUFBQTtBQUZKOztBQUtBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQUZKOztBQU1BO0VBQ0ksWUFBQTtBQUhKOztBQU1BO0VBQ0ksVUFBQTtBQUhKOztBQU9BO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFKSjs7QUFPQTtFQUNJLFVBQUE7RUFDRCxpQkFBQTtFQUNDLFlBQUE7RUFDQSxjQUFBO0FBSko7O0FBVUEsb0VBQUE7O0FBTUUsbURBQUE7O0FBTUUscURBQUE7O0FBQ0E7RUFFSTtJQUVJLFlBQUE7SUFDQSx3QkFBQTtJQUNBLDJIQUFBO0lBQ0Esc0JBQUE7SUFDQSxXQUFBO0lBQ0EsZ0JBQUE7RUFuQlY7O0VBc0JPO0lBQ0csWUFBQTtJQUNBLFdBQUE7SUFDQSxhQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0lBQ0EsaUJBQUE7SUFDQSxpQkFBQTtFQW5CVjs7RUF1QkU7SUFDSSxzQkFBQTtJQUNDLHFCQUFBO0lBQ0QsYUFBQTtJQUNBLFdBQUE7RUFwQk47QUFDRjs7QUF5Qkssb0VBQUE7O0FBS0MsaURBQUE7O0FBQ047RUFFSTtJQUVJLFlBQUE7SUFDQSx3QkFBQTtJQUNBLDJIQUFBO0lBQ0Esc0JBQUE7SUFDQSxXQUFBO0VBN0JOOztFQWlDRztJQUNHLFlBQUE7SUFDQSxXQUFBO0lBQ0EsYUFBQTtJQUNBLGVBQUE7SUFDQSxrQkFBQTtJQUNBLGlCQUFBO0lBQ0EsaUJBQUE7RUE5Qk47O0VBa0NGO0lBQ0ksc0JBQUE7SUFDQyxxQkFBQTtJQUNELGFBQUE7SUFDQSxXQUFBO0VBL0JGOztFQWtDRjtJQUNJLFVBQUE7SUFFQSxrQkFBQTtJQUVKLFNBQUE7SUFFQSxnQkFBQTtJQUNBLGVBQUE7RUFsQ0U7O0VBdUNGO0lBQ0ksVUFBQTtJQUNBLGdCQUFBO0VBcENGOztFQXVDRjtJQUNJLFVBQUE7RUFwQ0Y7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3N1Ym1pdDIvc3VibWl0Mi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jdmMtY2FyZCB7XHJcbiAgICBcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZpb2xldDtcclxuICAgIGJhY2tncm91bmQ6IHVybChcImh0dHBzOi8vY2RuLnBpeGFiYXkuY29tL3Bob3RvLzIwMTUvMDMvMjYvMDkvNDEvY29uZG9taW5pdW0tNjkwMDg2XzEyODAuanBnXCIpbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgZml4ZWQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gfVxyXG4gLmN2Yy1yZWFsdG9ye1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgcGFkZGluZy10b3A6IDEwcHg7XHJcbn1cclxuXHJcbiAuY3ZjLWhlYWRlcntcclxuICAgICBoZWlnaHQ6IDEwJTtcclxuIH1cclxuIC5jdmMtdGl0bGV7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDoxMHB4IDsgXHJcbiAgICBjb2xvcjogd2hpdGVzbW9rZTtcclxufVxyXG5cclxuLmN2Yy1jb250ZW50LCAuY3ZjLWhlYWRlcntcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCVcclxufVxyXG5cclxuLmN2Yy1mb3JtZmllbGR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIHBhZGRpbmc6IDVweDtcclxufVxyXG5cclxuI215U2VsZWN0SWR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuI2N2Yy1zZWFyY2h7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojMDA4MWM5O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG5cclxubGVmdDogMjAlO1xyXG5cclxucGFkZGluZzogNXB4IDJweDtcclxuZm9udC1zaXplOiAyMHB4O1xyXG5cclxufVxyXG5cclxuLmN2Yy1jaGVja2JveHtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuLnRlc3QtY29se1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgY29sb3I6YmxhY2s7XHJcbn1cclxuXHJcblxyXG4uY3ZjLWZvcm17XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBzbm93O1xyXG4gICAgIHdpZHRoOiA1MCUgIWltcG9ydGFudDtcclxuICAgIHBhZGRpbmc6MTBweDtcclxuICAgIGhlaWdodDogYXV0bztcclxufVxyXG5cclxuLmN2Yy1mb3JtbmFtZWVtYWlse1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uLWNvbG9yOiBibGFjaztcclxufVxyXG5cclxuI2N2Yy1jZW50dXJ5e1xyXG4gICAgd2lkdGg6NTAlO1xyXG59XHJcblxyXG4uY29sLW1kLTZ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG5cclxufVxyXG5cclxuLmNvbnRhaW5lcntcclxuICAgIGhlaWdodDogNTBweDtcclxufVxyXG5cclxuLmN2Yy1jaGVja2JveHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbn1cclxuXHJcblxyXG4uY3ZjLXJlYWx0b3IsIC5jdmMtYWxiaW5vbntcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4jY3ZjLWNlbnR1cnl7XHJcbiAgICB3aWR0aDo1MCU7XHJcbiAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAvL21hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG5cclxuXHJcbi8qIEV4dHJhIGxhcmdlIGRldmljZXMgKGxhcmdlIGxhcHRvcHMgYW5kIGRlc2t0b3BzLCAxMjAwcHggYW5kIHVwKSAqL1xyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOjEyMDBweCl7XHJcblxyXG5cclxufVxyXG5cclxuICAvKiBMYXJnZSBkZXZpY2VzIChsYXB0b3BzL2Rlc2t0b3BzLCA5OTJweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTJweCkge1xyXG5cclxuICB9XHJcblxyXG5cclxuICAgIC8qIE1lZGl1bSBkZXZpY2VzIChsYW5kc2NhcGUgdGFibGV0cywgNzY4cHggYW5kIHVwKSAqL1xyXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkge1xyXG5cclxuICAgICAgICAuY3ZjLWNhcmQge1xyXG4gICAgXHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdmlvbGV0O1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoXCJodHRwczovL2Nkbi5waXhhYmF5LmNvbS9waG90by8yMDE1LzAzLzI2LzA5LzQxL2NvbmRvbWluaXVtLTY5MDA4Nl8xMjgwLmpwZ1wiKW5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIGZpeGVkO1xyXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgIC5jdmMtdGl0bGV7XHJcbiAgICAgICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMHB4O1xyXG4gICAgICAgICAgICBwYWRkaW5nLXRvcDozMHB4IDsgXHJcbiAgICAgICAgICAgIGNvbG9yOiB3aGl0ZXNtb2tlO1xyXG4gICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgIFxyXG4gICAgLmN2Yy1mb3Jte1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHNub3c7XHJcbiAgICAgICAgIHdpZHRoOiA1MCUgIWltcG9ydGFudDtcclxuICAgICAgICBwYWRkaW5nOjEwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiA0MCU7XHJcbiAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAgLyogU21hbGwgZGV2aWNlcyAocG9ydHJhaXQgdGFibGV0cyBhbmQgbGFyZ2UgcGhvbmVzLCA2MDBweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xyXG5cclxuICB9XHJcblxyXG4gICAgICAvKiBFeHRyYSBzbWFsbCBkZXZpY2VzIChwaG9uZXMsIDYwMHB4IGFuZCBkb3duKSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcblxyXG4gICAgLmN2Yy1jYXJkIHtcclxuICAgIFxyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2aW9sZXQ7XHJcbiAgICAgICAgYmFja2dyb3VuZDogdXJsKFwiaHR0cHM6Ly9jZG4ucGl4YWJheS5jb20vcGhvdG8vMjAxNS8wMy8yNi8wOS80MS9jb25kb21pbml1bS02OTAwODZfMTI4MC5qcGdcIiluby1yZXBlYXQgY2VudGVyIGNlbnRlciBmaXhlZDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICBcclxuICAgICB9XHJcblxyXG4gICAgIC5jdmMtdGl0bGV7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcclxuICAgICAgICBwYWRkaW5nLXRvcDozMHB4IDsgXHJcbiAgICAgICAgY29sb3I6IHdoaXRlc21va2U7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbi5jdmMtZm9ybXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHNub3c7XHJcbiAgICAgd2lkdGg6IDgwJSAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzoxMHB4O1xyXG4gICAgaGVpZ2h0OiA0MCU7XHJcbn1cclxuXHJcbiNjdmMtc2VhcmNoe1xyXG4gICAgd2lkdGg6IDUwJTtcclxuXHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG5sZWZ0OiAyMCU7XHJcblxyXG5wYWRkaW5nOiA1cHggMnB4O1xyXG5mb250LXNpemU6IDIwcHg7XHJcblxyXG59XHJcblxyXG5cclxuI2N2Yy1jZW50dXJ5e1xyXG4gICAgd2lkdGg6NTAlO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuLmN2Yy1jaGVja2JveHtcclxuICAgIHdpZHRoOiAyMCU7XHJcbn1cclxuXHJcbiAgICBcclxuICAgIFxyXG5cclxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Submit2Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-submit2',
                templateUrl: './submit2.component.html',
                styleUrls: ['./submit2.component.scss']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }]; }, null); })();


/***/ }),

/***/ "./src/app/submit3/submit3.component.ts":
/*!**********************************************!*\
  !*** ./src/app/submit3/submit3.component.ts ***!
  \**********************************************/
/*! exports provided: Submit3Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Submit3Component", function() { return Submit3Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-intl-tel-input */ "./node_modules/ngx-intl-tel-input/__ivy_ngcc__/fesm2015/ngx-intl-tel-input.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var ng_recaptcha__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng-recaptcha */ "./node_modules/ng-recaptcha/__ivy_ngcc__/fesm2015/ng-recaptcha.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");












function Submit3Component_mat_error_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-error");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.getErrorMessage());
} }
class Submit3Component {
    constructor(router, formBuilder) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.email = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]);
        //tel = new FormControl (null, [Validators.required, Validators.pattern("[0-9 ]{12}")]);
        this.phone = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]);
        this.name = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
        this.recaptchaReactive = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required);
    }
    getErrorMessage() {
        if (this.email.hasError('required')) {
            return 'You must enter a value';
        }
        return this.email.hasError('email') ? 'Not a valid email' : '';
    }
    ngOnInit() {
        this.form = this.formBuilder.group({
            name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].email]],
            phone: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            // phone: new FormControl('', [Validators.required]),
            recaptchaReactive: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    }
    onSecondSubmit() {
        console.log(this.email.value, this.phone.value, this.name.value);
        console.log(this.form.value);
        const data2 = {
            "email": this.form.value.email,
            "phone": this.form.value.phone,
            "name": this.form.value.name
        };
        console.log(data2);
        localStorage.setItem('data2', JSON.stringify(data2));
        this.router.navigate(['/thankyou']);
    }
}
Submit3Component.ɵfac = function Submit3Component_Factory(t) { return new (t || Submit3Component)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"])); };
Submit3Component.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: Submit3Component, selectors: [["app-submit3"]], decls: 55, vars: 3, consts: [[1, "cvc-card"], [1, "cvc-header"], [1, "cvc-title"], [1, "cvc-content"], ["role", "form", 1, "cvc-form", 3, "formGroup"], ["appearance", "fill", 1, "cvc-formfield"], ["matInput", "", "formControlName", "name", "placeholder", "Ex. John Doe"], ["matInput", "", "placeholder", "test@gmail.com", "formControlName", "email", "required", ""], [4, "ngIf"], ["matInput", "", "type", "number", "formControlName", "phone", "required", "", "pattern", "^\\d*[0-9]\\d*$", "maxlength", "11"], ["formControlName", "recaptchaReactive"], ["type", "submit", "id", "cvc-search", "mat-raised-button", "", "color", "primary", 3, "disabled", "click"], [1, "container"], [1, "row"], [1, "col-md-3", "cvc-realtor"], ["routerLink", "/policy", "target", "_blank"], [1, "col-md-6"], ["mat-card-image", "", "id", "cvc-century", "src", "assets/images/century21-1.png", "alt", "Photo of Century21"], [1, "col-md-3", "cvc-albinon"]], template: function Submit3Component_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card-header", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "mat-card-title", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Nice place!! Please verify the details");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "mat-card-content", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-form-field", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Enter your name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "input", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "mat-form-field", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Enter your email");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, Submit3Component_mat_error_17_Template, 2, 1, "mat-error", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-form-field", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " Your Phone number");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "re-captcha", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function Submit3Component_Template_button_click_25_listener() { return ctx.onSecondSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Calculate my value");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "\u00A0\n");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Your Report will be");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, " prepared by a Realtor\u00AE.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Sam Farmaha Realtor\u00AE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, " Direct Line 416 951 2158.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Privacy Policy | Terms of Service");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "img", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, " 1780 Albinon Road,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, " Unit #3 ON M9V 1C1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Call: 416-742-8000");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Fax: 416-742-8001");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.email.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.form.valid);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardHeader"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardTitle"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardContent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormField"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatLabel"], _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInput"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_6__["NativeElementInjectorDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NumberValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["PatternValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["MaxLengthValidator"], ng_recaptcha__WEBPACK_IMPORTED_MODULE_8__["RecaptchaComponent"], ng_recaptcha__WEBPACK_IMPORTED_MODULE_8__["RecaptchaValueAccessorDirective"], _angular_material_button__WEBPACK_IMPORTED_MODULE_9__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCardImage"], _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatError"]], styles: [".cvc-card[_ngcontent-%COMP%] {\n  height: 100%;\n  background: url(\"https://cdn.pixabay.com/photo/2015/03/26/09/41/condominium-690086_1280.jpg\") no-repeat center center fixed;\n  background-size: cover;\n  width: 100%;\n  margin-left: 0px;\n}\n\n.cvc-header[_ngcontent-%COMP%] {\n  height: 20%;\n}\n\n.cvc-title[_ngcontent-%COMP%] {\n  margin: auto;\n  width: 100%;\n  padding: 10px;\n  font-size: 30px;\n  font-weight: 600px;\n  padding-top: 30px;\n  color: whitesmoke;\n}\n\n.cvc-content[_ngcontent-%COMP%], .cvc-header[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 100%;\n}\n\n.cvc-formfield[_ngcontent-%COMP%] {\n  width: 100%;\n  font-size: 18px;\n  align-content: center;\n  color: black;\n}\n\n#mySelectId[_ngcontent-%COMP%] {\n  width: 150%;\n  font-size: 18px;\n}\n\n.cvc-realtor[_ngcontent-%COMP%] {\n  font-size: 18px;\n}\n\n#cvc-search[_ngcontent-%COMP%] {\n  width: 50%;\n  position: relative;\n  top: 50%;\n  left: 20%;\n  margin-top: 10px;\n  padding: 5px 2px;\n  font-size: 20px;\n  background-color: #0081c9;\n}\n\n.cvc-checkbox[_ngcontent-%COMP%] {\n  font-size: 13px;\n}\n\n.test-col[_ngcontent-%COMP%] {\n  font-size: 15px;\n  color: black;\n}\n\n.cvc-form[_ngcontent-%COMP%] {\n  background-color: snow;\n  padding: 10px;\n  width: 50% !important;\n}\n\n.cvc-formnameemail[_ngcontent-%COMP%] {\n  -webkit-text-decoration-color: black;\n          text-decoration-color: black;\n}\n\n#cvc-century[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-top: 10px;\n}\n\n.col-md-6[_ngcontent-%COMP%] {\n  text-align: center;\n  max-width: 100%;\n  margin-top: 10px;\n}\n\n.cvc-realtor[_ngcontent-%COMP%], .cvc-albinon[_ngcontent-%COMP%] {\n  font-size: 17px;\n  padding-top: 10px;\n  text-align: center;\n}\n\n#cvc-century[_ngcontent-%COMP%] {\n  width: 50%;\n  padding-top: 10px;\n  margin: auto;\n  display: block;\n}\n\n\n\n\n\n\n\n\n\n\n\n@media only screen and (max-width: 600px) {\n  .cvc-card[_ngcontent-%COMP%] {\n    height: 100%;\n    background-color: violet;\n    background: url(\"https://cdn.pixabay.com/photo/2015/03/26/09/41/condominium-690086_1280.jpg\") no-repeat center center fixed;\n    background-size: cover;\n    width: 100%;\n    margin-left: 0px;\n  }\n\n  .cvc-title[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    padding: 10px;\n    font-size: 23px;\n    font-weight: 600px;\n    padding-top: 30px;\n    color: whitesmoke;\n  }\n\n  .cvc-form[_ngcontent-%COMP%] {\n    background-color: snow;\n    width: 90%;\n    padding: 10px;\n    width: 70% !important;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 50%;\n    padding-top: 10px;\n  }\n\n  #cvc-search[_ngcontent-%COMP%] {\n    width: 70%;\n    position: relative;\n    top: 50%;\n    left: 10%;\n    margin-top: 10px;\n    padding: 5px 2px;\n    font-size: 20px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VibWl0My9zdWJtaXQzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUVBLDJIQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFBSjs7QUFHQztFQUNJLFdBQUE7QUFBTDs7QUFFQztFQUNHLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0o7O0FBR0E7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7RUFDQyxRQUFBO0VBQ0wsU0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7QUFBQTs7QUFJQTtFQUNJLGVBQUE7QUFESjs7QUFJQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0FBREo7O0FBS0E7RUFDSSxzQkFBQTtFQUVBLGFBQUE7RUFDQSxxQkFBQTtBQUhKOztBQU1BO0VBQ0ksb0NBQUE7VUFBQSw0QkFBQTtBQUhKOztBQUtBO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0FBRko7O0FBS0E7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUZKOztBQU1BO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFISjs7QUFNQTtFQUNJLFVBQUE7RUFDRCxpQkFBQTtFQUNDLFlBQUE7RUFDQSxjQUFBO0FBSEo7O0FBT0Esb0VBQUE7O0FBTUUsbURBQUE7O0FBTUUscURBQUE7O0FBTUMsb0VBQUE7O0FBS0MsaURBQUE7O0FBQ047RUFFSTtJQUVJLFlBQUE7SUFDQSx3QkFBQTtJQUNBLDJIQUFBO0lBQ0Esc0JBQUE7SUFDQSxXQUFBO0lBQ0EsZ0JBQUE7RUF6Qk47O0VBNEJHO0lBQ0csWUFBQTtJQUNBLFdBQUE7SUFDQSxhQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0lBQ0EsaUJBQUE7SUFDQSxpQkFBQTtFQXpCTjs7RUE0QkU7SUFDSSxzQkFBQTtJQUNBLFVBQUE7SUFDQSxhQUFBO0lBQ0EscUJBQUE7RUF6Qk47O0VBMkJFO0lBQ0ksVUFBQTtJQUNBLGlCQUFBO0VBeEJOOztFQTJCRTtJQUNJLFVBQUE7SUFDQSxrQkFBQTtJQUNDLFFBQUE7SUFDTCxTQUFBO0lBQ0EsZ0JBQUE7SUFDQSxnQkFBQTtJQUNBLGVBQUE7RUF4QkY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3N1Ym1pdDMvc3VibWl0My5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jdmMtY2FyZCB7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgIC8vIGJhY2tncm91bmQtY29sb3I6IHZpb2xldDtcclxuICAgIGJhY2tncm91bmQ6IHVybChcImh0dHBzOi8vY2RuLnBpeGFiYXkuY29tL3Bob3RvLzIwMTUvMDMvMjYvMDkvNDEvY29uZG9taW5pdW0tNjkwMDg2XzEyODAuanBnXCIpbm8tcmVwZWF0IGNlbnRlciBjZW50ZXIgZml4ZWQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gfVxyXG5cclxuIC5jdmMtaGVhZGVye1xyXG4gICAgIGhlaWdodDogMjAlO1xyXG4gfVxyXG4gLmN2Yy10aXRsZXtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcclxuICAgIHBhZGRpbmctdG9wOjMwcHggO1xyXG4gICAgY29sb3I6IHdoaXRlc21va2U7XHJcbn1cclxuXHJcbi5jdmMtY29udGVudCwgLmN2Yy1oZWFkZXJ7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlXHJcbn1cclxuXHJcbi5jdmMtZm9ybWZpZWxke1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbiNteVNlbGVjdElke1xyXG4gICAgd2lkdGg6IDE1MCU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbi5jdmMtcmVhbHRvcntcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIC8vcGFkZGluZy10b3A6IDVweDtcclxufVxyXG5cclxuI2N2Yy1zZWFyY2h7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgIHRvcDogNTAlO1xyXG5sZWZ0OiAyMCU7XHJcbm1hcmdpbi10b3A6IDEwcHg7XHJcbnBhZGRpbmc6IDVweCAycHg7XHJcbmZvbnQtc2l6ZTogMjBweDtcclxuYmFja2dyb3VuZC1jb2xvcjogIzAwODFjOTtcclxuXHJcbn1cclxuXHJcbi5jdmMtY2hlY2tib3h7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbi50ZXN0LWNvbHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGNvbG9yOmJsYWNrO1xyXG59XHJcblxyXG5cclxuLmN2Yy1mb3Jte1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogc25vdztcclxuICAgIFxyXG4gICAgcGFkZGluZzoxMHB4O1xyXG4gICAgd2lkdGg6IDUwJSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY3ZjLWZvcm1uYW1lZW1haWx7XHJcbiAgICB0ZXh0LWRlY29yYXRpb24tY29sb3I6IGJsYWNrO1xyXG59XHJcbiNjdmMtY2VudHVyeXtcclxuICAgIHdpZHRoOjUwJTtcclxuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uY29sLW1kLTZ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcblxyXG5cclxuLmN2Yy1yZWFsdG9yLCAuY3ZjLWFsYmlub257XHJcbiAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuI2N2Yy1jZW50dXJ5e1xyXG4gICAgd2lkdGg6NTAlO1xyXG4gICBwYWRkaW5nLXRvcDogMTBweDtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgLy9tYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuLyogRXh0cmEgbGFyZ2UgZGV2aWNlcyAobGFyZ2UgbGFwdG9wcyBhbmQgZGVza3RvcHMsIDEyMDBweCBhbmQgdXApICovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6MTIwMHB4KXtcclxuXHJcblxyXG59XHJcblxyXG4gIC8qIExhcmdlIGRldmljZXMgKGxhcHRvcHMvZGVza3RvcHMsIDk5MnB4IGFuZCB1cCkgKi9cclxuICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDk5MnB4KSB7XHJcblxyXG4gIH1cclxuXHJcblxyXG4gICAgLyogTWVkaXVtIGRldmljZXMgKGxhbmRzY2FwZSB0YWJsZXRzLCA3NjhweCBhbmQgdXApICovXHJcbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KSB7XHJcblxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAgLyogU21hbGwgZGV2aWNlcyAocG9ydHJhaXQgdGFibGV0cyBhbmQgbGFyZ2UgcGhvbmVzLCA2MDBweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xyXG5cclxuICB9XHJcblxyXG4gICAgICAvKiBFeHRyYSBzbWFsbCBkZXZpY2VzIChwaG9uZXMsIDYwMHB4IGFuZCBkb3duKSAqL1xyXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDYwMHB4KSB7XHJcblxyXG4gICAgLmN2Yy1jYXJkIHtcclxuXHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZpb2xldDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB1cmwoXCJodHRwczovL2Nkbi5waXhhYmF5LmNvbS9waG90by8yMDE1LzAzLzI2LzA5LzQxL2NvbmRvbWluaXVtLTY5MDA4Nl8xMjgwLmpwZ1wiKW5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIGZpeGVkO1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDBweDtcclxuICAgICB9XHJcblxyXG4gICAgIC5jdmMtdGl0bGV7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcclxuICAgICAgICBwYWRkaW5nLXRvcDozMHB4IDtcclxuICAgICAgICBjb2xvcjogd2hpdGVzbW9rZTtcclxuICAgIH1cclxuXHJcbiAgICAuY3ZjLWZvcm17XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogc25vdztcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIHBhZGRpbmc6MTBweDtcclxuICAgICAgICB3aWR0aDogNzAlICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgICAjY3ZjLWNlbnR1cnl7XHJcbiAgICAgICAgd2lkdGg6NTAlO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgICNjdmMtc2VhcmNoe1xyXG4gICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgICB0b3A6IDUwJTtcclxuICAgIGxlZnQ6IDEwJTtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICBwYWRkaW5nOiA1cHggMnB4O1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIFxyXG5cclxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Submit3Component, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-submit3',
                templateUrl: './submit3.component.html',
                styleUrls: ['./submit3.component.scss']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }, { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }]; }, null); })();


/***/ }),

/***/ "./src/app/thankyou/thankyou.component.ts":
/*!************************************************!*\
  !*** ./src/app/thankyou/thankyou.component.ts ***!
  \************************************************/
/*! exports provided: ThankyouComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThankyouComponent", function() { return ThankyouComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");






class ThankyouComponent {
    constructor(authService) {
        this.authService = authService;
        this.formattedAddress = "";
        this.options = {
            componentRestrictions: {
                country: ['IND', 'CA', 'USA']
            }
        };
    }
    handleAddressChange(address) {
        this.formattedAddress = address.formatted_address;
        console.log(this.formattedAddress);
    }
    ngOnInit() {
        let data = {
            "email": JSON.parse(localStorage.getItem('data2')).email,
            "name": JSON.parse(localStorage.getItem('data2')).name,
            "location": JSON.parse(localStorage.getItem('useraddress')),
            "mobile": JSON.parse(localStorage.getItem('data2')).phone,
            "bedrooms": JSON.parse(localStorage.getItem('data1')).bedrooms,
            "selltime": JSON.parse(localStorage.getItem('data1')).sell,
            "sqfeet": JSON.parse(localStorage.getItem('data1')).sq,
            "bath": JSON.parse(localStorage.getItem('data1')).bath,
            "unit": JSON.parse(localStorage.getItem('unitdata')).unit
        };
        console.log(data);
        this.authService.Sendemail(data).subscribe(data => {
            window.alert('Your details are on the way to our team');
        });
    }
}
ThankyouComponent.ɵfac = function ThankyouComponent_Factory(t) { return new (t || ThankyouComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"])); };
ThankyouComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ThankyouComponent, selectors: [["app-thankyou"]], decls: 59, vars: 0, consts: [[1, "container"], [1, "row"], [1, "col", "cvc-request", 2, "margin-top", "20px"], [1, "cvc-condo"], [1, "row", "cvc-details"], [1, "col-md-4", "cvc-details"], [2, "text-align", "center"], [1, "col-md-4", 2, "text-align", "center"], ["mat-card-image", "", "id", "cvc-century", "src", "assets/images/century21-1.png", "alt", "Photo of Century21"], [1, "col-md-4"], ["id", "container2", "rowHeight", "1:1", 1, "container"], [1, "cvc-subtitle22"], [1, "cvc-title22"], [1, "cvc-card3"], [1, "cvc-title3", 2, "text-align", "center"], [1, "cvc-subtitle3"], [1, "cvc-content3"], [1, "cvc-button3"], ["mat-button", "", "routerLink", "/", 2, "font", "size 25px", "text-align", "center"]], template: function ThankyouComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "We have received your request, report will be prepared by a Realtor\u00AE");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h3", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "We may seek other realtor\u2019s help to evaluate your Condo, if the request generated outside of Sam Farmaha\u2019s Jurisdiction");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "strong");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Sam Farmaha");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Call: 416-915-2158 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " Email:farmaha@look.ca ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "h2", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, " 1780 Albinon Road,");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " Unit #2 #3 TORONTO ONTARIO M9V1C1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "mat-card-subtitle", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "strong");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " Buying Help ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-card-content", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, " If you\u2019re buying, I can enhance your search process by helping you uncover what you may not have considered, whether it is schools, lifestyle, or the investment value of the property. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "mat-card-subtitle", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "strong");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " Selling Help");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "mat-card-content", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "If you\u2019re selling, I will educate you on all market factors that may influence price, preparation and the promotion of your property so you can make an informed decision that best benefits your needs.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "mat-card-subtitle", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "strong");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, " New Construction ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "mat-card-content", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "The REALTOR\u2019s\u00AE expertise helps facilitate the transaction, saving clients time and money and avoiding problems. Real estate is a regulated profession in Ontarioso all salespersons and brokers must be registered with the Real Estate Council of Ontario (RECO). ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "mat-card", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "mat-card-title", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Thank you for vising my website I am here to provide my services and just a phone call away, you are under no obligation. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "\u00A0 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "mat-card-subtitle", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Disclaimer, Privacy Policy and Prohibited Use of the Site and Content. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "mat-card-content", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, " *Not intended to solicit Buyers or Sellers already under contract, by using the Site and its mobile versions, and by subscribing to email services you agree to comply with the Agreement and confirm that you have a bona fide interest in buying, selling or leasing a residential property. If you do not agree to comply with the Agreement, you may not use the Site or subscribe to email services. You are prohibited Use of the Site and Content to Invade another person\u2019s privacy or collect, disclose or store personal information about other persons, including telephone numbers, street addresses, email addresses, last names, etc. The materials on this website are provided \u201Cas is\u201D. We do not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on this website or on any websites linked to this site. We have no control over the authenticity of the content on display. In this website Realtor word is used to describe a sales Representative, REALTOR\u00AE logo are certification marks owned by REALTOR\u00AE Canada Inc., a corporation jointly owned by the National Association of REALTORS\u00AE and CREA. The REALTOR\u00AE trademarks are used to identify real estate services provided by brokers and salespersons who are members of CREA and who accept and respect a strict Code of Ethics, and are required to meet consistent professional standards of business practice which is the consumer\u2019s assurance of integrity. These Terms of Use shall be governed by the laws of Ontario, Canada. All users of this website are bound by these conditions and should therefore periodically visit this page to review any changes to these requirements. The users continued use of this website following the posting of any changes constitutes acceptance by the user of such modifications. Sincere Thanks for your visit. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "mat-card-actions", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Back to main page");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardImage"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardSubtitle"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardContent"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCard"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardTitle"], _angular_material_card__WEBPACK_IMPORTED_MODULE_2__["MatCardActions"], _angular_material_button__WEBPACK_IMPORTED_MODULE_3__["MatButton"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"]], styles: ["@media screen and (min-width: 1200px) {\n  .cvc-content1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    padding: 10px;\n    text-align: center;\n    font-size: 50px;\n    font-weight: 600px;\n    color: whitesmoke;\n  }\n\n  .col-md-6[_ngcontent-%COMP%] {\n    width: 50%;\n    text-align: center;\n    font-size: 10px;\n    font-weight: 600px;\n  }\n\n  .cvc-subtitle[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    text-align: center;\n    font-size: 18px;\n    font-weight: 400px;\n    padding: 10px;\n    color: whitesmoke;\n    margin-top: 10px;\n  }\n\n  .cvc-content[_ngcontent-%COMP%], .cvc-header[_ngcontent-%COMP%], .container[_ngcontent-%COMP%] {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: 100%;\n  }\n\n  .container[_ngcontent-%COMP%] {\n    margin-top: 10px;\n  }\n\n  #cvc-sam[_ngcontent-%COMP%] {\n    height: 100px;\n  }\n\n  .cvc-content2[_ngcontent-%COMP%] {\n    text-align: center;\n    font-size: 18px;\n    font-weight: 400;\n  }\n\n  .cvc-title3[_ngcontent-%COMP%] {\n    font-style: italic;\n  }\n\n  .cvc-title3[_ngcontent-%COMP%], .cvc-subtitle3[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n\n  .cvc-subtitle3[_ngcontent-%COMP%] {\n    font-size: 20px;\n    font-weight: 500;\n  }\n\n  .cvc-content3[_ngcontent-%COMP%] {\n    padding: 20px;\n    text-align: center;\n  }\n\n  .cvc-button3[_ngcontent-%COMP%] {\n    font-size: 40px;\n    font-weight: 600;\n    text-align: center;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 100%;\n    height: 100px;\n    padding: 0px;\n    text-align: center;\n    margin-top: 20%;\n  }\n\n  .cvc-subtitle22[_ngcontent-%COMP%], .cvc-title22[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n\n  .cvc-subtitle22[_ngcontent-%COMP%] {\n    color: black;\n    font-size: 18px;\n    font-weight: 400;\n  }\n\n  .col-md-3[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n\n  .custom-image[_ngcontent-%COMP%] {\n    display: block;\n    margin-left: auto;\n    margin-right: auto;\n    width: 100%;\n    margin-top: 0px;\n  }\n\n  .social-button[_ngcontent-%COMP%] {\n    font-size: 20px;\n    color: #0066ff;\n    background-color: white;\n    margin-top: 10px;\n    text-align: center;\n  }\n}\n\n@media only screen and (min-width: 992px) {\n  .cvc-content1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    padding: 0px;\n    text-align: center;\n    font-size: 50px;\n    font-weight: 600px;\n    padding-top: 0px;\n    color: whitesmoke;\n  }\n\n  .col-md-6[_ngcontent-%COMP%] {\n    width: 50%;\n    text-align: center;\n    font-size: 10px;\n    font-weight: 600px;\n  }\n\n  .cvc-subtitle[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    text-align: center;\n    padding: 10px;\n    font-size: 18px;\n    font-weight: 400px;\n    padding-top: 0px;\n    color: whitesmoke;\n  }\n\n  .cvc-request[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n\n  .cvc-condo[_ngcontent-%COMP%] {\n    margin-top: -30px;\n  }\n\n  .cvc-content[_ngcontent-%COMP%], .cvc-header[_ngcontent-%COMP%], .container[_ngcontent-%COMP%] {\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: 100%;\n  }\n\n  .container[_ngcontent-%COMP%] {\n    margin-top: 10px;\n  }\n\n  #cvc-sam[_ngcontent-%COMP%] {\n    height: 100px;\n  }\n\n  .cvc-content2[_ngcontent-%COMP%] {\n    text-align: center;\n    font-size: 18px;\n    font-weight: 400;\n  }\n\n  .cvc-title3[_ngcontent-%COMP%], .cvc-subtitle3[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n\n  .cvc-subtitle3[_ngcontent-%COMP%] {\n    font-size: 20px;\n    font-weight: 500;\n  }\n\n  .cvc-content3[_ngcontent-%COMP%] {\n    padding: 20px;\n    text-align: center;\n  }\n\n  .cvc-button3[_ngcontent-%COMP%] {\n    font-size: 40px;\n    font-weight: 600;\n    text-align: center;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 100%;\n    height: 70px;\n    padding: 0px;\n    text-align: center;\n  }\n\n  .cvc-subtitle22[_ngcontent-%COMP%], .cvc-title22[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n\n  .cvc-subtitle22[_ngcontent-%COMP%] {\n    color: black;\n    font-size: 18px;\n    font-weight: 400;\n  }\n\n  .custom-image[_ngcontent-%COMP%] {\n    display: block;\n    margin-left: auto;\n    margin-right: auto;\n    width: 100%;\n    margin-top: 0px;\n  }\n\n  .social-button[_ngcontent-%COMP%] {\n    font-size: 20px;\n    color: #0066ff;\n    background-color: white;\n    margin-top: 10px;\n  }\n}\n\n@media only screen and (min-width: 768px) {\n  .cvc-request[_ngcontent-%COMP%], .cvc-details[_ngcontent-%COMP%] {\n    text-align: center;\n    margin-top: 20px;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 50%;\n    height: 70px;\n    padding: 0px;\n    text-align: center;\n  }\n}\n\n@media only screen and (min-width: 600px) {\n  .cvc-request[_ngcontent-%COMP%] {\n    text-align: center;\n    margin-top: 20px;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 50%;\n    height: 70px;\n    padding: 0px;\n    text-align: center;\n  }\n}\n\n@media only screen and (max-width: 600px) {\n  .cvc-title[_ngcontent-%COMP%], .cvc-content1[_ngcontent-%COMP%] {\n    margin: auto;\n    width: 100%;\n    padding-bottom: 0px;\n    text-align: center;\n    font-size: 25px;\n    font-weight: 600px;\n    padding-top: 0px;\n    color: whitesmoke;\n  }\n\n  .custom-image[_ngcontent-%COMP%] {\n    display: block;\n    margin-left: auto;\n    margin-right: auto;\n    width: 38%;\n    margin-top: 0px;\n  }\n\n  .cvc-subtitle22[_ngcontent-%COMP%] {\n    color: black;\n    font-size: 20px;\n    font-weight: 400;\n  }\n\n  .cvc-content4[_ngcontent-%COMP%] {\n    width: 50%;\n    text-align: center;\n    font-size: 10px;\n    font-weight: 600px;\n  }\n\n  .cvc-subtitle3[_ngcontent-%COMP%] {\n    font-size: 15px;\n    font-weight: 500;\n  }\n\n  .cvc-subtitle22[_ngcontent-%COMP%], .cvc-title22[_ngcontent-%COMP%] {\n    text-align: center;\n    font-size: 18px;\n  }\n\n  .col-md-6[_ngcontent-%COMP%] {\n    width: 100%;\n    text-align: center;\n    font-size: 10px;\n    font-weight: 600px;\n  }\n\n  .cvc-request[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n\n  #cvc-century[_ngcontent-%COMP%] {\n    width: 50%;\n    height: 70px;\n    padding: 0px;\n    text-align: center;\n  }\n\n  .cvc-button3[_ngcontent-%COMP%] {\n    text-align: center;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGhhbmt5b3UvdGhhbmt5b3UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsb0VBQUE7QUFDQTtFQUVDO0lBQ0csWUFBQTtJQUNBLFdBQUE7SUFDQSxhQUFBO0lBQ0Esa0JBQUE7SUFDQSxlQUFBO0lBQ0Esa0JBQUE7SUFFQSxpQkFBQTtFQURGOztFQUtGO0lBQ00sVUFBQTtJQUNBLGtCQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0VBRko7O0VBT0Y7SUFDSSxZQUFBO0lBQ0EsV0FBQTtJQUNBLGtCQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLGlCQUFBO0lBQ0EsZ0JBQUE7RUFKRjs7RUFTRjtJQUNJLGFBQUE7SUFDQSx1QkFBQTtJQUNBLG1CQUFBO0lBQ0EsV0FBQTtFQU5GOztFQVNGO0lBQ0csZ0JBQUE7RUFORDs7RUFVRjtJQUNJLGFBQUE7RUFQRjs7RUFXQTtJQUNDLGtCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0VBUkQ7O0VBVUE7SUFDRSxrQkFBQTtFQVBGOztFQVVGO0lBQ0ksa0JBQUE7RUFQRjs7RUFXRjtJQUNJLGVBQUE7SUFDQSxnQkFBQTtFQVJGOztFQVlGO0lBQ0ksYUFBQTtJQUNBLGtCQUFBO0VBVEY7O0VBWUY7SUFDSSxlQUFBO0lBQ0EsZ0JBQUE7SUFDQSxrQkFBQTtFQVRGOztFQVlGO0lBQ0ksV0FBQTtJQUNBLGFBQUE7SUFDQSxZQUFBO0lBQ0Esa0JBQUE7SUFDQSxlQUFBO0VBVEY7O0VBY0Y7SUFDSSxrQkFBQTtFQVhGOztFQWNGO0lBQ0ksWUFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtFQVhGOztFQWNGO0lBQ0ksa0JBQUE7RUFYRjs7RUFlRjtJQUNJLGNBQUE7SUFDQSxpQkFBQTtJQUNBLGtCQUFBO0lBQ0EsV0FBQTtJQUNBLGVBQUE7RUFaRjs7RUFlRjtJQUNJLGVBQUE7SUFDQSxjQUFBO0lBQ0EsdUJBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VBWkY7QUFDRjtBQW9CRSxtREFBQTtBQUNBO0VBR0M7SUFDSyxZQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7SUFDQSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxrQkFBQTtJQUNBLGdCQUFBO0lBQ0EsaUJBQUE7RUFwQk47O0VBd0JFO0lBQ00sVUFBQTtJQUNBLGtCQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0VBckJSOztFQXlCRTtJQUNJLFlBQUE7SUFDQSxXQUFBO0lBQ0Esa0JBQUE7SUFDQSxhQUFBO0lBQ0EsZUFBQTtJQUNBLGtCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxpQkFBQTtFQXRCTjs7RUF5QkU7SUFDSSxrQkFBQTtFQXRCTjs7RUF5QkU7SUFDSSxpQkFBQTtFQXRCTjs7RUEwQkU7SUFDSSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSxtQkFBQTtJQUNBLFdBQUE7RUF2Qk47O0VBMEJFO0lBQ0csZ0JBQUE7RUF2Qkw7O0VBMkJFO0lBQ0ksYUFBQTtFQXhCTjs7RUE0Qkk7SUFDQyxrQkFBQTtJQUNBLGVBQUE7SUFDQSxnQkFBQTtFQXpCTDs7RUE2QkU7SUFDSSxrQkFBQTtFQTFCTjs7RUE2QkU7SUFDSSxlQUFBO0lBQ0EsZ0JBQUE7RUExQk47O0VBOEJFO0lBQ0ksYUFBQTtJQUNBLGtCQUFBO0VBM0JOOztFQThCRTtJQUNJLGVBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VBM0JOOztFQThCRTtJQUNJLFdBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0VBM0JOOztFQWdDRTtJQUNJLGtCQUFBO0VBN0JOOztFQWdDRTtJQUNJLFlBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7RUE3Qk47O0VBaUNFO0lBQ0ksY0FBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7SUFDQSxXQUFBO0lBQ0EsZUFBQTtFQTlCTjs7RUFpQ0U7SUFDSSxlQUFBO0lBQ0EsY0FBQTtJQUNBLHVCQUFBO0lBQ0EsZ0JBQUE7RUE5Qk47QUFDRjtBQW9DSSxxREFBQTtBQUNBO0VBQ0k7SUFDSSxrQkFBQTtJQUNBLGdCQUFBO0VBbENWOztFQXFDTTtJQUNJLFVBQUE7SUFDQSxZQUFBO0lBQ0EsWUFBQTtJQUNBLGtCQUFBO0VBbENWO0FBQ0Y7QUF3Q0ssb0VBQUE7QUFDSDtFQUNFO0lBQ0ksa0JBQUE7SUFDQSxnQkFBQTtFQXRDTjs7RUF5Q0U7SUFDSSxVQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSxrQkFBQTtFQXRDTjtBQUNGO0FBNENNLGlEQUFBO0FBQ047RUFDSTtJQUNJLFlBQUE7SUFDQSxXQUFBO0lBQ0EsbUJBQUE7SUFDQSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxrQkFBQTtJQUNBLGdCQUFBO0lBQ0EsaUJBQUE7RUExQ047O0VBOENFO0lBQ0ksY0FBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7SUFDQSxVQUFBO0lBQ0EsZUFBQTtFQTNDTjs7RUE2Q0U7SUFDSSxZQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0VBMUNOOztFQTZDRTtJQUNJLFVBQUE7SUFDQSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxrQkFBQTtFQTFDTjs7RUE4Q0Y7SUFDSSxlQUFBO0lBQ0EsZ0JBQUE7RUEzQ0Y7O0VBaURGO0lBQ0ksa0JBQUE7SUFDQSxlQUFBO0VBOUNGOztFQWlERjtJQUNJLFdBQUE7SUFDQSxrQkFBQTtJQUNBLGVBQUE7SUFDQSxrQkFBQTtFQTlDRjs7RUFpREY7SUFDSSxrQkFBQTtFQTlDRjs7RUFnREY7SUFDSSxVQUFBO0lBQ0EsWUFBQTtJQUNBLFlBQUE7SUFDQSxrQkFBQTtFQTdDRjs7RUFpREY7SUFDSSxrQkFBQTtFQTlDRjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvdGhhbmt5b3UvdGhhbmt5b3UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBFeHRyYSBsYXJnZSBkZXZpY2VzIChsYXJnZSBsYXB0b3BzIGFuZCBkZXNrdG9wcywgMTIwMHB4IGFuZCB1cCkgKi9cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDoxMjAwcHgpe1xyXG5cclxuIC5jdmMtY29udGVudDF7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcclxuXHJcbiAgICBjb2xvcjp3aGl0ZXNtb2tlO1xyXG59XHJcblxyXG5cclxuLmNvbC1tZC02e1xyXG4gICAgICB3aWR0aDogNTAlO1xyXG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgZm9udC13ZWlnaHQ6IDYwMHB4O1xyXG59XHJcblxyXG5cclxuXHJcbi5jdmMtc3VidGl0bGV7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246Y2VudGVyIDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDBweDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBjb2xvcjogd2hpdGVzbW9rZTtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuLmN2Yy1jb250ZW50LCAuY3ZjLWhlYWRlciwgLmNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6ZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCVcclxufVxyXG5cclxuLmNvbnRhaW5lcntcclxuICAgbWFyZ2luLXRvcDogMTBweDtcclxuXHJcbn1cclxuXHJcbiNjdmMtc2Fte1xyXG4gICAgaGVpZ2h0OjEwMHB4O1xyXG4gIFxyXG4gIH1cclxuICBcclxuICAuY3ZjLWNvbnRlbnQye1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICB9XHJcbiAgLmN2Yy10aXRsZTN7XHJcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgfVxyXG5cclxuLmN2Yy10aXRsZTMsIC5jdmMtc3VidGl0bGUze1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICBcclxufVxyXG5cclxuLmN2Yy1zdWJ0aXRsZTN7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG5cclxufVxyXG5cclxuLmN2Yy1jb250ZW50M3tcclxuICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5jdmMtYnV0dG9uM3tcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbiNjdmMtY2VudHVyeXtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IDIwJTtcclxuXHJcbn1cclxuXHJcblxyXG4uY3ZjLXN1YnRpdGxlMjIsIC5jdmMtdGl0bGUyMntcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLmN2Yy1zdWJ0aXRsZTIye1xyXG4gICAgY29sb3I6YmxhY2s7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcblxyXG4uY29sLW1kLTN7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcblxyXG4uY3VzdG9tLWltYWdle1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG59XHJcblxyXG4uc29jaWFsLWJ1dHRvbntcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjMDA2NmZmO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxufVxyXG5cclxuICAvKiBMYXJnZSBkZXZpY2VzIChsYXB0b3BzL2Rlc2t0b3BzLCA5OTJweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA5OTJweCkge1xyXG5cclxuICAgIFxyXG4gICAuY3ZjLWNvbnRlbnQxe1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcclxuICAgICAgICBwYWRkaW5nLXRvcDowcHggO1xyXG4gICAgICAgIGNvbG9yOndoaXRlc21va2U7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIFxyXG4gICAgLmNvbC1tZC02e1xyXG4gICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDBweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgXHJcbiAgICAuY3ZjLXN1YnRpdGxle1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOmNlbnRlciA7XHJcbiAgICAgICAgcGFkZGluZzogMTBweDtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMHB4O1xyXG4gICAgICAgIHBhZGRpbmctdG9wOjBweCA7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlc21va2U7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jdmMtcmVxdWVzdHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLmN2Yy1jb25kb3tcclxuICAgICAgICBtYXJnaW4tdG9wOi0zMHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIC5jdmMtY29udGVudCwgLmN2Yy1oZWFkZXIsIC5jb250YWluZXJ7XHJcbiAgICAgICAgZGlzcGxheTpmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6MTAwJVxyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuY29udGFpbmVye1xyXG4gICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIFxyXG4gICAgfVxyXG4gICAgXHJcbiAgICAjY3ZjLXNhbXtcclxuICAgICAgICBoZWlnaHQ6MTAwcHg7XHJcbiAgICAgIFxyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAuY3ZjLWNvbnRlbnQye1xyXG4gICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIC5jdmMtdGl0bGUzLCAuY3ZjLXN1YnRpdGxlM3tcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jdmMtc3VidGl0bGUze1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgXHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jdmMtY29udGVudDN7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jdmMtYnV0dG9uM3tcclxuICAgICAgICBmb250LXNpemU6IDQwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgICNjdmMtY2VudHVyeXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIFxyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIC5jdmMtc3VidGl0bGUyMiwgLmN2Yy10aXRsZTIye1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmN2Yy1zdWJ0aXRsZTIye1xyXG4gICAgICAgIGNvbG9yOmJsYWNrO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIC5jdXN0b20taW1hZ2V7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLnNvY2lhbC1idXR0b257XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGNvbG9yOiAjMDA2NmZmO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICB9XHJcbiAgICBcclxuXHJcbiAgfVxyXG5cclxuXHJcbiAgICAvKiBNZWRpdW0gZGV2aWNlcyAobGFuZHNjYXBlIHRhYmxldHMsIDc2OHB4IGFuZCB1cCkgKi9cclxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAgICAgICAuY3ZjLXJlcXVlc3QsIC5jdmMtZGV0YWlsc3tcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgICAgIH0gIFxyXG5cclxuICAgICAgICAjY3ZjLWNlbnR1cnl7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogNzBweDtcclxuICAgICAgICAgICAgcGFkZGluZzogMHB4O1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAgLyogU21hbGwgZGV2aWNlcyAocG9ydHJhaXQgdGFibGV0cyBhbmQgbGFyZ2UgcGhvbmVzLCA2MDBweCBhbmQgdXApICovXHJcbiAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xyXG4gICAgLmN2Yy1yZXF1ZXN0e1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgICNjdmMtY2VudHVyeXtcclxuICAgICAgICB3aWR0aDogNTAlO1xyXG4gICAgICAgIGhlaWdodDogNzBweDtcclxuICAgICAgICBwYWRkaW5nOiAwcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgXHJcbiAgICB9XHJcbiAgICBcclxuXHJcbiAgfVxyXG5cclxuICAgICAgLyogRXh0cmEgc21hbGwgZGV2aWNlcyAocGhvbmVzLCA2MDBweCBhbmQgZG93bikgKi9cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xyXG4gICAgLmN2Yy10aXRsZSwuY3ZjLWNvbnRlbnQxe1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246Y2VudGVyO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgICAgICBmb250LXdlaWdodDogNjAwcHg7XHJcbiAgICAgICAgcGFkZGluZy10b3A6MHB4IDtcclxuICAgICAgICBjb2xvcjp3aGl0ZXNtb2tlO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAuY3VzdG9tLWltYWdle1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICB3aWR0aDogMzglO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIH1cclxuICAgIC5jdmMtc3VidGl0bGUyMntcclxuICAgICAgICBjb2xvcjpibGFjaztcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIH1cclxuXHJcbiAgICAuY3ZjLWNvbnRlbnQ0e1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgICAgICBmb250LXdlaWdodDogNjAwcHg7XHJcbiAgfVxyXG5cclxuICBcclxuLmN2Yy1zdWJ0aXRsZTN7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG5cclxufVxyXG5cclxuXHJcblxyXG4uY3ZjLXN1YnRpdGxlMjIsIC5jdmMtdGl0bGUyMntcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuLmNvbC1tZC02e1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwcHg7XHJcbn1cclxuXHJcbi5jdmMtcmVxdWVzdHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4jY3ZjLWNlbnR1cnl7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxufVxyXG5cclxuLmN2Yy1idXR0b24ze1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbiAgICBcclxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ThankyouComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-thankyou',
                templateUrl: './thankyou.component.html',
                styleUrls: ['./thankyou.component.scss']
            }]
    }], function () { return [{ type: _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\sange\OneDrive\Desktop\gitlab\checkyourpropertyvalue\checkyourpropertyvalue-frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map